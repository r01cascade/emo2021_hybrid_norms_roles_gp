from fitness.base_ff_classes.base_ff import base_ff
import hashlib # to create hash from ind string
from algorithm.parameters import params # for folder name
import os # for cd class, path exist
from distutils.dir_util import mkpath, copy_tree, remove_tree # create, copy, delete temp Model folder
import subprocess # to execute shell script

import random # DEV
from collections import OrderedDict
from datetime import datetime

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class cascade_hybridMed(base_ff):
    """
    A simulation is executed via shell script.
    Fitness is read from a text file.
    """

    def __init__(self):
        # Initialise base fitness function class.
        super().__init__()

    def replace_mediator_code(self, phenotype, folder_path):
        # replace follows this order because there are overlaps in variable names
        phenotype_dict = OrderedDict([
                #('RoleDisp','RoleDisp'),
                #('NormDisp','NormDisp'),
                ('0.333','1.0/3.0'),
                ('mediatedFirstDrink','mediatedDoubleFirstDrink'),
                ('mediated2Drinks','mediatedDoubleNextDrink'),
                ('mediated3to4Drinks','mediatedDoubleNextDrink'),
                ('mediated5to7Drinks','mediatedDoubleNextDrink'),
                ('mediated8to11Drinks','mediatedDoubleNextDrink'),
                ('mediated12MoreDrinks','mediatedDoubleNextDrink'),

                #('MediatedGatewayDouble','mediatedGatewayDouble'), # From mediator?
                #('MediatedVector[j]','mediatedVector[j]'),
            ])
        str = phenotype
        for bnf_text, c_variable in phenotype_dict.items():
            str = str.replace(bnf_text, c_variable)
        str = str.split(' ')
        str_first_drink = str[0]+'\n'
        str_2_drinks = str[1]+'\n'
        str_3_4_drinks = str[2]+'\n'
        str_5_7_drinks = str[3]+'\n'
        str_8_11_drinks = str[4]+'\n'
        str_12_more_drinks = str[5]+'\n'

        # # modify model.props. If needed, turn mechanisms OFF (assume that ON is the default)
        # model_props_file =  folder_path + '/roles/props/model.props'
        # new_file = folder_path + '/roles/src/RolesTheory.cpp'
        # with open(model_props_file) as f:
        #     new_source = f.read()
        #     if 'OFF' in str[0]:
        #         new_source = new_source.replace('role.tp.beta1=0.12351','role.tp.beta1=0')
        #         new_source = new_source.replace('role.tp.beta2=3.09795','role.tp.beta2=0')
        #     if 'OFF' in str[1]:
        #         new_source = new_source.replace('role.socialisation.on=1','role.socialisation.on=0')
        #     new_source = new_source.replace('stop.at=7300','stop.at=10950') ### CHANGE SIMULATED YEARS (need to change in calculateFitness.R)
        # with open(model_props_file, 'w') as f:
        #     f.write(new_source)

        # modify calculate fitness to 30 SIMLATED YEARS
        R_file =  folder_path + '/calculateFitness.R'
        with open(R_file) as f:
            new_source = f.read()
            new_source = new_source.replace('N_YEARS <- 20','N_YEARS <- 30') ### CHANGE SIMULATED YEARS (need to change in calculateFitness.R)
        with open(R_file, 'w') as f:
            f.write(new_source)
        
        # insert lines of codes to the simulation code
        template_file = folder_path + '/hybrid/src/EliMediator.template.cpp'
        new_file = folder_path + '/hybrid/src/EliMediator.cpp'
        cpp_dict = OrderedDict([
                ('//GE-FIRST-DRINK',str_first_drink),
                ('//GE-2-DRINKS',str_2_drinks),
                ('//GE-3-4-DRINKS',str_3_4_drinks),
                ('//GE-5-7-DRINKS',str_5_7_drinks),
                ('//GE-8-11-DRINKS',str_8_11_drinks),
                ('//GE-12-more-DRINKS',str_12_more_drinks),
            ])
        with open(template_file) as f:
            new_source = f.read()
            for code_marking, code_line in cpp_dict.items():
                new_source = new_source.replace(code_marking, code_line)
        with open(new_file, 'w') as f:
            f.write(new_source)


    def evaluate(self, ind, **kwargs):
        # Generate hash with md5 from phenotype
        hash_object = hashlib.md5((ind.phenotype+str(datetime.now())).encode())
        folder_path = hash_object.hexdigest()

        # Create a model folder
        folder_path = params['MODELS_FOLDER'] + '/' + folder_path
        mkpath(folder_path)

        # Copy simulation into a model folder
        copy_tree(params['SIMULATION_FOLDER'], folder_path)

        # Edit roles theory with phenotype
        
        self.replace_mediator_code(ind.phenotype, folder_path)

        #fitness = random.uniform(0,1) # DEV
        
        # use cd class (for context manager) to execute the simulation
        # outside the context manager we are back wherever we started.
        with cd(folder_path):
            # Run a shell script: run RepastHPC simulation, and R script for fitness, wait
            process = subprocess.Popen('bash compileThenRun.sh', shell=True, stdout=subprocess.PIPE)
            process.wait()
            
            # Read fitness from file
            if process.returncode != 0: #if error
                fitness = base_ff.default_fitness # bad fitness
            else:
                # Read fitness from file
                f = open("fitness.out", "r")
                fitness = float(f.readline())
            '''
            print('=================')
            print(ind.phenotype.replace('; ',';\n'))
            print('=> Fitness: ', fitness)
            print('=================')
            '''
        
        # Delete a model folder
        remove_tree(folder_path)
        
        return fitness
