from fitness.base_ff_classes.base_ff import base_ff


class cascade_nodes(base_ff):
    """
    Fitness function class for minimising the number of nodes in a
    derivation tree.
    """

    def __init__(self):
        # Initialise base fitness function class.
        super().__init__()
        
    def evaluate(self, ind, **kwargs):
        # minimise number of nodes, but count ON as 2 (instead of 1 like OFF)
        return ind.nodes + ind.phenotype.count('ON')
