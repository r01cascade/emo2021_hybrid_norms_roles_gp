# About
This is the source code for the simulation model and the model discovery process (grammatical evolution) by [CASCADE project](https://www.sheffield.ac.uk/cascade) for a publication on EMO 2021 conference.

*Title:* Using Multi-objective Grammar-based Genetic Programming to Integrate Multiple Social Theories in Agent-based Modeling

*Authours:* Tuong M. Vu, Eli Davies, Charlotte Buckley, Alan Brennan, and Robin C. Purshouse

This repo is hereby licensed for use under the GNU GPL version 3.

# Folders and files structure
* The Model folder is the source code of the hybrid simulation model. The hybrid norms and roles model needs to be compiled before starting the model discovery process.
* The model discovery process uses PonyGE2 which needs to be downloaded separately (instruction below). The four files (cascade\_hybridMed.bnf, cascade\_hybridMed.txt, cascade\_hybridMed.py, and cascade\_nodes.py) are to be used with PonyGE2.
    * cascade\_hybridMed.txt defines the settings of the model discovery process.
    * cascade\_hybridMed.bnf defines the grammar.
    * cascade\_hybridMed.py calculates model errors by comparing the simulated outputs with the real-world target data.
    * cascade\_nodes.py calculates the complexity of the structure (the number of nodes with a special case of ON node counted as 2).
* In the setup below, we will set up a gehybrid folder. In gehybrid folder, there will be the PonyGE2 source code, the Model folder. We will also create a Models folder to contain temporary copies of the simulation model during evaluation of the model discovery process.

# Compile and run the best-calibrated model by human modellers
* Install [RepastHPC](https://repast.github.io/repast_hpc.html).
* Update the Model/hybrid/env file to match with the RepastHPC installation.
* In the terminal, change directory to Model/hybrid then execute the command: make all

# Set up and run Grammatical Evolution with PonyGE2
* Download [PonyGE2 source code](https://github.com/PonyGE/PonyGE2)
* Assuming the name of the downloaded folder is gehybrid.
* Create a folder Models in the folder you just downloaded (If the source files is in gehybrid/src then the new foler should be gehybrid/Models).
* Move the **compiled** human model folder into gehybrid/Model (The location of two folders core and roles should be: gehybrid/Model/core and gehybrid/Model/roles).
* Move cascade\_hybridMed.bnf to gehybrid/grammars folder.
* Move cascade\_hybridMed.txt to gehybrid/parameters folder. Make sure the path in this files match with your setup (currently /home/username/...).
* Move cascade\_hybridMed.py and cascade\_nodes.py into gehybrid/src/fitness
* Run the model discovery process by navigating to gehybrid/src then typing this command: python3 ponyge.py --parameters cascade\_hybridMed.txt
