#ifndef INCLUDE_NORMMODEL_H_
#define INCLUDE_NORMMODEL_H_

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"

#include "Model.h"
#include "NormReferenceGroup.h"
#include "RegulatorInjunctiveBingePunishment.h" //for 1 core debug outputs
#include "RegulatorInjunctiveRelaxation.h" //for 1 core debug outputs

class NormModel : public Model {
private:
	int mIndexAutonomy; //index of autonomy read from file

	int mIntervalDesNorm;
	int mIntervalPunish;
	int mIntervalRelax;

	ofstream annualNormOutput; //for 1 core debug outputs
	std::string addUniqueSuffix(std::string fileName); //for 1 core debug outputs
	RegulatorInjunctiveBingePunishment* mpRegPunishment; //for 1 core debug outputs
	RegulatorInjunctiveRelaxation* mpRegRelaxation; //for 1 core debug outputs

public:
	NormModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	~NormModel();

	void initMediatorAndTheoryWithRandomParameters(Agent *agent) override;
	void initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info) override;
	void initForTheory(repast::ScheduleRunner& runner) override;
	void readRankFileForTheory(std::string rankFileName) override;
	
	void writeAnnualAgentDataToFile() override;
};


#endif /* INCLUDE_NORMMODEL_H_ */
