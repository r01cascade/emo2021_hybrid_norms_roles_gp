#ifndef NORM_THEORY_H_
#define NORM_THEORY_H_

#include "Theory.h"
#include "Agent.h"
#include "repast_hpc/SharedContext.h"
#include "InjunctiveNormEntity.h"
#include "DescriptiveNormEntity.h"
#include "NormReferenceGroup.h"

#include <math.h>
#include <boost/mpi.hpp>
#include <boost/lexical_cast.hpp>
#include "repast_hpc/logger.h"

class NormTheory : public Theory {

private:
    double  		 mAutonomy;
    double 			 mDesireGateway;
    double       mDesireMean;
    double       mDesireSd;
    double  		 mDescriptiveNormGate;
    double  		 mDescriptiveNormQuant;
    double  		 mDescriptiveNormQuantSd;
    repast::SharedContext<Agent> *mpPopulation;
    InjunctiveNormEntity *mpInjNormEntity;
    DescriptiveNormEntity *mpDesNormEntity;
    double mOneTimeRandomForDisposition;
    
    int mReferenceGroupId;

/*    //This function takes a list of drinkingValues range:(0,1) calculated by calcCurrentDrinking and returns a number of drinks
    //consumed in the drinking session. It also takes a bool isDeterministic so that whatever calls the method
    //can decide wether to use the deterministic version of the method or the probablistic version.
    int doDrinkingEngine(std::vector<double> drinkingValues, bool isDeterministic = true);

    //overloaded version of doDrinkingEngine that takes a single double drinkingValue range (0,1) (produced by the calcCurrentDrinking method)
    //a bool on whether to use the determinisitic or probabilitistic version of the code, and returns a number of drinks consumed: 1 or 0.
    //isDeterministic is defaulted to TRUE, so you can call the function with just the drinkingValue argument.
    int doDrinkingEngine(double drinkingValue, bool isDeterministic = true);
*/
    /* Situational mechanisms */
    void calcDescriptiveNorm();

    int findAgeGroup();
    double calcDispositionFunc(double autonomy, double payoff, double injunctive, double descriptive);

public:
    NormTheory(repast::SharedContext<Agent> *pPopulation, InjunctiveNormEntity *pInjNormEntity,
			DescriptiveNormEntity *pDesNormEntity);
	NormTheory(repast::SharedContext<Agent> *pPopulation, InjunctiveNormEntity *pInjNormEntity,
			DescriptiveNormEntity *pDesNormEntity, double autonomy);
	~NormTheory();

    void doSituation() override;
    double doGatewayDisposition() override;
    std::vector<double> doNextDrinksDisposition() override;
    void doNonDrinkingActions() override;

    void initDesires();
};

#endif /* NORM_THEORY_H_ */
