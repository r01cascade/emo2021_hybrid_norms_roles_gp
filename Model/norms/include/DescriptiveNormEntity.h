#ifndef INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_
#define INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_

#include "StructuralEntity.h"
#include "repast_hpc/SharedContext.h"
#include "Agent.h"

class DescriptiveNormEntity : public StructuralEntity {

private:
	repast::SharedContext<Agent> *mpContext;

	double* mAvgIsDrinking;
	double* mAvgNumberDrinks;
	double* mSdNumberDrinks;

public:
	DescriptiveNormEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList,
			int transformationalInterval,
			repast::SharedContext<Agent> *context);
	~DescriptiveNormEntity();
	void doTransformation() override;

	void updateDescriptiveGroupDrinking();
	double getAvgIsDrinking(int groupId);
	double getAvgNumberDrinks(int groupId);
	double getSdNumberDrinks(int groupId);
};

#endif /* INCLUDE_DESCRIPTIVE_NORM_ENTITY_H_ */
