#include "RegulatorInjunctiveRelaxation.h"
#include "NormGlobals.h"

RegulatorInjunctiveRelaxation::RegulatorInjunctiveRelaxation(repast::SharedContext<Agent> *context) {
	mpContext = context;

	//create a 2D array: noRow x noCol
	int size = P_REFERENCE_GROUP->size();
	adjustmentLevelsGamma = new double[size];
	adjustmentLevelsLambda = new double[size];
	mTransformationalTriggerCount = new int[size];
	
	resetCount();
}

RegulatorInjunctiveRelaxation::~RegulatorInjunctiveRelaxation() {
	delete[] adjustmentLevelsGamma;
	delete[] adjustmentLevelsLambda;
	delete[] mTransformationalTriggerCount;
}

void RegulatorInjunctiveRelaxation::resetCount() {
	for (int i=0; i<P_REFERENCE_GROUP->size(); ++i)
		mTransformationalTriggerCount[i] = 0;
}

void RegulatorInjunctiveRelaxation::resetAdjustmentLevel() {
	for (int i=0; i<P_REFERENCE_GROUP->size(); ++i) {
		adjustmentLevelsGamma[i] = 1;
		adjustmentLevelsLambda[i] = 1;
	}
}

void RegulatorInjunctiveRelaxation::updateAdjustmentLevel() {
	int size = P_REFERENCE_GROUP->size();

	//init array, reset to 0
	int countN [size];
	int countCurrentDrinker [size];
	for (int i=0; i<size; ++i) {
		countN[i] = 0;
		countCurrentDrinker[i] = 0;
	}

	//loop through agent and count num of heavy drinkers
	repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = mpContext->localEnd();
	while (iter != iterEnd) {
		++countN[P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup())];
		if ((*iter)->isHaveKDrinksOverNDays(COMP_DAYS_RELAX, 1)) {
			++countCurrentDrinker[P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup())];
		}
		iter++;
	}

	for (int i=0; i<size; ++i) {
		double proportionCurrentDrinkers = 0;
		if (countN[i] != 0){
			proportionCurrentDrinkers = double(countCurrentDrinker[i]) / double(countN[i]);
		} else {
			//std::cout << "There are no people in this demographic here" << std::endl;
		}

		//std::cout << "== descriptive proportion drinkers: " << i << " " << j << " " << proportionCurrentDrinkers << std::endl;
		if (proportionCurrentDrinkers > mpInjunctiveNormEntity->getInjNormGamma(i)) {
			mTransformationalTriggerCount[i]++;
			adjustmentLevelsGamma[i] = INJ_RELAXATION_GAMMA_ADJUSTMENT;
			adjustmentLevelsLambda[i] = INJ_RELAXATION_LAMBDA_ADJUSTMENT;
			//std::cout << "The injunctive norm was adjusted (relax): " << i << " " << j << std::endl;
		} else {
			adjustmentLevelsGamma[i] = 1;
			adjustmentLevelsLambda[i] = 1;
			//std::cout << "The injunctive norm was unchanged: " << i << " " << j << " " << mpInjunctiveNormEntity->getInjNormGate(i,j) << std::endl;
		}
	}
/* #ifdef DEBUG
				std::stringstream msg;
				msg
						<< "Heavy drinkers had to be punished. The new injunctive norm of this group is now: "
						<< mpInjunctiveNorms[currentSex][currentAgeGroup]
						<< "\n";
				std::cout << msg.str();
#endif */


}

void RegulatorInjunctiveRelaxation::setInjNormEntity(InjunctiveNormEntity *pInjNormEntity) {
	mpInjunctiveNormEntity = pInjNormEntity;
}

double RegulatorInjunctiveRelaxation::getAdjustmentLevelGamma(int groupId) {
	return adjustmentLevelsGamma[groupId];
}

double RegulatorInjunctiveRelaxation::getAdjustmentLevelLambda(int groupId) {
	return adjustmentLevelsLambda[groupId];
}
