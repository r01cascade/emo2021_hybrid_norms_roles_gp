#include "repast_hpc/Utilities.h"
#include "repast_hpc/RepastProcess.h"

#include <vector>

#include "globals.h"
#include "NormGlobals.h"
#include "InjunctiveNormEntity.h"
#include "Agent.h"
#include "RegulatorInjunctiveBingePunishment.h"
#include "RegulatorInjunctiveRelaxation.h"

InjunctiveNormEntity::InjunctiveNormEntity(
		std::vector<Regulator*> regulatorList, std::vector<double> powerList, int intervalPunish, int intervalRelax,
		std::vector<std::string> normDataGate,
		std::vector<std::string> normDataGamma, std::vector<std::string> normDataLambda) :
			StructuralEntity(regulatorList, powerList, 1) { //mTransformationalInterval = 1
	mIntervalPunish = intervalPunish;
	mIntervalRelax = intervalRelax;

	//set a pointer to this entity in the relaxation regulator
	((RegulatorInjunctiveRelaxation*) mpRegulatorList[1])->setInjNormEntity(this);

	
	//create a 2D array
	int size = P_REFERENCE_GROUP->size();
	mpInjunctiveNormsGate = new double[size];
	mpInjunctiveNormsGamma = new double[size];
	mpInjunctiveNormsLambda = new double[size];


	///////////
	//put data into array
	std::vector<std::string>::const_iterator injunct_iter_gate = normDataGate.begin();
	int currentIndex = 0;

	//read values from global data
	while (injunct_iter_gate != normDataGate.end()){
		mpInjunctiveNormsGate[currentIndex] = repast::strToDouble(*injunct_iter_gate);
		currentIndex++;
		injunct_iter_gate++;
	}
	if (currentIndex != size){
		std::cerr <<  "Missmatch in inj norm gate data read in and size intended\n";
		int Missmatch = size-currentIndex;
		std::cout << "size = " << size << endl;
		std::cout << "currentIndex = " << currentIndex << endl;
		if(currentIndex < size)
		{
			std::cerr <<  "\n P_REFERENCE_GROUP size Bigger than currentIndex";
		}
	}
	///////////


	///////////
	//put data into array
	std::vector<std::string>::const_iterator injunct_iter_gamma = normDataGamma.begin();
	currentIndex = 0;

	//read values from global data
	while (injunct_iter_gamma != normDataGamma.end()){
		mpInjunctiveNormsGamma[currentIndex] = repast::strToDouble(*injunct_iter_gamma);
		currentIndex++;
		injunct_iter_gamma++;
	}
	if (currentIndex != size){
		std::cerr <<  "Missmatch in inj norm gate data read in and size intended";
	}
	///////////


	///////////
	// Trying to read in the injunctive norm data for quantity.
	std::vector<std::string>::const_iterator injunct_iter_lambda = normDataLambda.begin();
	currentIndex = 0;
	
	while (injunct_iter_lambda != normDataLambda.end()){
		mpInjunctiveNormsLambda[currentIndex] = repast::strToDouble(*injunct_iter_lambda);
		currentIndex++;
		injunct_iter_lambda++;
	}
	if (currentIndex != size){
		std::cerr <<  "Missmatch in inj norm quant data read in and size intended";
	}
	///////////
}

InjunctiveNormEntity::~InjunctiveNormEntity() {
	delete[] mpInjunctiveNormsGate;
	delete[] mpInjunctiveNormsGamma;
	delete[] mpInjunctiveNormsLambda;
}

double InjunctiveNormEntity::getInjNormGate(int groupId) {
	return mpInjunctiveNormsGate[groupId];
}

double InjunctiveNormEntity::getInjNormGamma(int groupId) {
	return mpInjunctiveNormsGamma[groupId];
}

double InjunctiveNormEntity::getInjNormLambda(int groupId) {
	return mpInjunctiveNormsLambda[groupId];
}

void InjunctiveNormEntity::doTransformation() {
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (currentTick % mTransformationalInterval == 0) {
		mTransformationalTriggerCount++;

		//call all regulators to update adjustment value
		RegulatorInjunctiveBingePunishment* pRegPunish = (RegulatorInjunctiveBingePunishment*) mpRegulatorList[0];
		RegulatorInjunctiveRelaxation* pRegRelax = (RegulatorInjunctiveRelaxation*) mpRegulatorList[1];

		if (currentTick % mIntervalPunish == 0)
			pRegPunish->updateAdjustmentLevel();
		else
			pRegPunish->resetAdjustmentLevel();

		if (currentTick % mIntervalRelax == 0)
			pRegRelax->updateAdjustmentLevel();
		else
			pRegRelax->resetAdjustmentLevel();

		//update injunctive norm based on adjustment level values from regulators
		int size = P_REFERENCE_GROUP->size();
		for (int groupId = 0; groupId != size; ++groupId) {
			mpInjunctiveNormsGate[groupId] *=
				( mpPowerList[0] * pRegPunish->getAdjustmentLevelGamma(groupId) +
				mpPowerList[1] * pRegRelax->getAdjustmentLevelGamma(groupId) );
			if (mpInjunctiveNormsGate[groupId] > 1) {
				mpInjunctiveNormsGate[groupId] = 1;
			}

			mpInjunctiveNormsGamma[groupId] *=
				( mpPowerList[0] * pRegPunish->getAdjustmentLevelGamma(groupId) +
				mpPowerList[1] * pRegRelax->getAdjustmentLevelGamma(groupId) );
			if (mpInjunctiveNormsGamma[groupId] > 1) {
				mpInjunctiveNormsGamma[groupId] = 1;
			}

			mpInjunctiveNormsLambda[groupId] *=
				( mpPowerList[0] * pRegPunish->getAdjustmentLevelLambda(groupId) +
				mpPowerList[1] * pRegRelax->getAdjustmentLevelLambda(groupId) );
		}

		// EXPERIMENT 3
		/*if(repast::RepastProcess::instance()->getScheduleRunner().currentTick() >= 365*5 && !mInterventionFlag){
			//std::cout << "==B== " << repast::RepastProcess::instance()->getScheduleRunner().currentTick() << std::endl;
			for (int currentSex = 0; currentSex != NUM_SEX; ++currentSex) {
				for (int currentAgeGroup = 0; currentAgeGroup != NUM_AGE_GROUPS; ++currentAgeGroup) {
					//std::cout << currentSex <<"\t" << currentAgeGroup <<"\t" <<mpInjunctiveNormsGate[currentSex][currentAgeGroup];
					mpInjunctiveNormsGate[currentSex][currentAgeGroup] /= 2;
					//std::cout << "\t" << mpInjunctiveNormsGate[currentSex][currentAgeGroup] << std::endl;
				}
			}
			//std::cout << "==E== " << repast::RepastProcess::instance()->getScheduleRunner().currentTick() << std::endl << std::endl;
			mInterventionFlag = true;
		}*/
	}
}
