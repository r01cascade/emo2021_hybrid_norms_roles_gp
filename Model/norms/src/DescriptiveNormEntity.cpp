#include "DescriptiveNormEntity.h"
#include "globals.h"
#include "NormGlobals.h"

#include <vector>
#include "repast_hpc/Utilities.h"
#include "repast_hpc/RepastProcess.h"
#include <math.h>
#include <stdio.h>

DescriptiveNormEntity::DescriptiveNormEntity(
		std::vector<Regulator*> regulatorList, std::vector<double> powerList, int transformationalInterval,
		repast::SharedContext<Agent> *context) :
			StructuralEntity(regulatorList, powerList, transformationalInterval) {
	mpContext = context;

	//create a 2D arrays
	int size = P_REFERENCE_GROUP->size();
	mAvgIsDrinking = new double[size];
	mAvgNumberDrinks = new double[size];
	mSdNumberDrinks = new double[size];
	
	//init arrays from global values
	for (int i=0; i<size; ++i) {
		mAvgIsDrinking[i] = 0;
		mAvgNumberDrinks[i] = 0;
		mSdNumberDrinks[i] = 0;
	}
}

DescriptiveNormEntity::~DescriptiveNormEntity() {
	delete[] mAvgIsDrinking;
	delete[] mAvgNumberDrinks;
	delete[] mSdNumberDrinks;
}

void DescriptiveNormEntity::updateDescriptiveGroupDrinking() {
	int size = P_REFERENCE_GROUP->size();

	//reset avg arrays
	for (int i=0; i<size; ++i) {
		mAvgIsDrinking[i] = 0;
		mAvgNumberDrinks[i] = 0;
		mSdNumberDrinks[i] = 0;
	}

	//create arrays to count agents in a group
	int* pCountInGroup = new int[size];
	int* pCountDrinkersInGroup = new int[size];
	for(int i = 0; i < size; ++i) {
		pCountInGroup[i] = 0;
		pCountDrinkersInGroup[i] = 0;
	}

	//loop through agents to calculate average
	std::vector<Agent*> agents;
	mpContext->selectAgents(mpContext->size(), agents);
	std::vector<Agent*>::const_iterator iter = agents.begin();
	std::vector<Agent*>::const_iterator iterEnd = agents.end();
	while (iter != iterEnd) {
		int sex = (*iter)->getSex();
		int ageGroup = (*iter)->findAgeGroup();
		int roleNum = (*iter)->getNumberOfRoles();
		int groupId = P_REFERENCE_GROUP->getId(sex, ageGroup);

		//mAvgIsDrinking[sex][ageGroup] += (*iter)->isDrinkingToday();
		mAvgIsDrinking[groupId] += ((double)(*iter)->getNumDaysHavingKDrinksOverNDays(N_DAYS_DESCRIPTIVE, 1)) / (double)N_DAYS_DESCRIPTIVE;
		pCountInGroup[groupId]++;

		if ((*iter)->isHaveKDrinksOverNDays(N_DAYS_DESCRIPTIVE,1)) {
			pCountDrinkersInGroup[groupId]++;
			mAvgNumberDrinks[groupId] += (*iter)->getAvgDrinksNDays(N_DAYS_DESCRIPTIVE, true);
			//std::cout << "sum of average drinks per occasion " << mAvgNumberDrinks[sex][ageGroup] << std::endl;
		}

		iter++;
	}

	//loop again to calculate standard deviation
	iter = agents.begin();
	iterEnd = agents.end();
	while (iter != iterEnd) {
		int sex = (*iter)->getSex();
		int ageGroup = (*iter)->findAgeGroup();
		int roleNum = (*iter)->getNumberOfRoles();
		int groupId = P_REFERENCE_GROUP->getId(sex, ageGroup);

		if ((*iter)->isHaveKDrinksOverNDays(N_DAYS_DESCRIPTIVE ,1)) {
			//pCountInGroup[sex][ageGroup]++;
			mSdNumberDrinks[groupId] += pow( (*iter)->getAvgDrinksNDays(N_DAYS_DESCRIPTIVE, true) - mAvgNumberDrinks[groupId] ,2);
			//printf("%.2f\t%.2f\t%.2f\n",(*iter)->getAvgDrinksNDays(N_DAYS_DESCRIPTIVE),mAvgNumberDrinks[sex][ageGroup],mSdNumberDrinks[sex][ageGroup]);
		} else {
			//printf("%.s\t%.s\t%.2f\n","_","_",mSdNumberDrinks[sex][ageGroup]);
		}

		iter++;
	}

	//caculate aggregate info
	for (int i=0; i<size; ++i) {
		if (pCountInGroup[i] == 0)
			mAvgIsDrinking[i] = 0;
		else mAvgIsDrinking[i] /= pCountInGroup[i];

		if (pCountDrinkersInGroup[i] == 0) {
			mAvgNumberDrinks[i] = 0;
			mSdNumberDrinks[i] = 0;
		} else {
			//double tempBefore = mSdNumberDrinks[i][j];
			mAvgNumberDrinks[i] /= pCountDrinkersInGroup[i];
			mSdNumberDrinks[i] = sqrt(mSdNumberDrinks[i] / pCountDrinkersInGroup[i]);
			//printf("=== %d\t%f\t%f\n",pCountDrinkersInGroup[i][j],tempBefore,mSdNumberDrinks[i][j]);
		}
	}

	//delete count array
	delete[] pCountInGroup;
	delete[] pCountDrinkersInGroup;
}

double DescriptiveNormEntity::getAvgIsDrinking(int groupId) {
	return mAvgIsDrinking[groupId];
}

double DescriptiveNormEntity::getAvgNumberDrinks(int groupId) {
	return mAvgNumberDrinks[groupId];
}

double DescriptiveNormEntity::getSdNumberDrinks(int groupId) {
	return mSdNumberDrinks[groupId];
}

void DescriptiveNormEntity::doTransformation() {
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (currentTick % mTransformationalInterval == 0) {
		mTransformationalTriggerCount++;

		updateDescriptiveGroupDrinking(); //update avg drinking values
	}
}
