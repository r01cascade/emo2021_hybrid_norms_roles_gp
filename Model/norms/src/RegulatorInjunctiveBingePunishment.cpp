#include "RegulatorInjunctiveBingePunishment.h"
#include "NormGlobals.h"

RegulatorInjunctiveBingePunishment::RegulatorInjunctiveBingePunishment(repast::SharedContext<Agent> *context) {
	mpContext = context;

	//create a 2D array: noRow x noCol
	int size = P_REFERENCE_GROUP->size();
	adjustmentLevelsGamma = new double[size];
	adjustmentLevelsLambda = new double[size];
	mTransformationalTriggerCount = new int[size];

	resetCount();
}

RegulatorInjunctiveBingePunishment::~RegulatorInjunctiveBingePunishment() {
	delete[] adjustmentLevelsGamma;
	delete[] adjustmentLevelsLambda;
	delete[] mTransformationalTriggerCount;
}

void RegulatorInjunctiveBingePunishment::resetCount() {
	for (int i=0; i<P_REFERENCE_GROUP->size(); ++i)
		mTransformationalTriggerCount[i] = 0;
}

void RegulatorInjunctiveBingePunishment::resetAdjustmentLevel() {
	for (int i=0; i<P_REFERENCE_GROUP->size(); ++i) {
		adjustmentLevelsGamma[i] = 1;
		adjustmentLevelsLambda[i] = 1;
	}
}

void RegulatorInjunctiveBingePunishment::updateAdjustmentLevel() {
	int size = P_REFERENCE_GROUP->size();

	//init array, reset to 0
	int countN[size] = {0};
	int countHeavyDrinker[size] = {0};
	for (int i=0; i<size; ++i) {
		countN[i] = 0;
		countHeavyDrinker[i] = 0;
	}

	//loop through agent and count num of heavy drinkers
	repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = mpContext->localEnd();
	while (iter != iterEnd) {
		++countN[P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup())];
		if ((*iter)->getAvgDrinksNDays(COMP_DAYS_PUNISH, false)>INJUNCTIVE_THRESHOLD) {
			++countHeavyDrinker[P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup())];
		}
		iter++;
	}

	//update adjustment levels
	for (int i=0; i<size; ++i) {
		double proportionHeavyDrinkers;
		if (countN[i] !=0) {
			proportionHeavyDrinkers = double(countHeavyDrinker[i]) / double(countN[i]);
		} else {
			//std::cout << "There are no people in this demographic here" << std::endl;
		}

		if (proportionHeavyDrinkers > INJUNCTIVE_PROPORTION) {
			mTransformationalTriggerCount[i]++;
			adjustmentLevelsGamma[i] = INJ_PUNISHMENT_GAMMA_ADJUSTMENT;
			adjustmentLevelsLambda[i] = INJ_PUNISHMENT_LAMBDA_ADJUSTMENT;
			//std::cout << "The injunctive norm was adjusted (punish): " << i << " " << j << std::endl;
		} else {
			adjustmentLevelsGamma[i] = 1;
			adjustmentLevelsLambda[i] = 1;
		}
	}

/* #ifdef DEBUG
			std::stringstream msg;
			msg
					<< "Heavy drinkers had to be punished. The new injunctive norm of this group is now: "
					<< mpInjunctiveNorms[currentSex][currentAgeGroup]
					<< "\n";
			std::cout << msg.str();
#endif */

}

double RegulatorInjunctiveBingePunishment::getAdjustmentLevelGamma(int groupId) {
	return adjustmentLevelsGamma[groupId];
}

double RegulatorInjunctiveBingePunishment::getAdjustmentLevelLambda(int groupId) {
	return adjustmentLevelsLambda[groupId];
}
