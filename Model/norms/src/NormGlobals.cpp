#include "NormGlobals.h"

int INJUNCTIVE_THRESHOLD = 999; // this is read in from the model.props
double INJUNCTIVE_PROPORTION = 99.9; // this is read in from the model.props
double INJUNCTIVE_ADJUSTMENT = 99.9; // this is read in from the model.props
double INJ_RELAXATION_GAMMA_ADJUSTMENT = 1.05; // this is read in from the model.props
double INJ_RELAXATION_LAMBDA_ADJUSTMENT = 0.95; // this is read in from the model.props
double INJ_PUNISHMENT_GAMMA_ADJUSTMENT = 0.95; // this is read in from the model.props
double INJ_PUNISHMENT_LAMBDA_ADJUSTMENT = 1.05; // this is read in from the model.props

// Todo: this needs to be specified for the descriptive entity
const int DESCRIPTIVE_INCUBATION_PERIOD = 100;
const double DESCRIPTIVE_INCUBATION_PERCENT = 0.85;

// This is the bias in perceiving quantity
double PERCEPTION_BIAS = 0.5; // this is read in from model.props

// Time periods over which drinking behaviour it is evaluated
int N_DAYS_DESCRIPTIVE = 30; // is used to generate descriptive norms on prevalence and average quantity
int COMP_DAYS_PUNISH = 90; // Used in the binge punishment (average drinks in comp days > injunctive threshold)
int COMP_DAYS_RELAX = 90; // Used in the injunctive relaxation (prevalence of one drink over comp days)

const double DEFAULT_D_NORM = 0.5;
const double DEFAULT_D_NORM_QUANT = 0.5;
const double DEFAULT_D_NORM_QUANT_SD = 0.1;

// CP Edit: add the discounting factor to decrease payoff with increasing drinks
double DISCOUNT_MALE = 0.1;
double DISCOUNT_FEMALE = 0.1;

double DESIRE_MULTIPLIER_ABSTAINER = 0.5;
double DESIRE_MULTIPLIER_DRINKER = 1.5;

NormReferenceGroup *P_REFERENCE_GROUP = new NormReferenceGroup();