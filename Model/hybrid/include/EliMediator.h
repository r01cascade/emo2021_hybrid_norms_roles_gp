#ifndef ELI_MEDIATOR_H_
#define ELI_MEDIATOR_H_

#include "Theory.h"
#include "TheoryMediator.h"
#include <vector>

class EliMediator: public TheoryMediator{

protected:
	std::vector<double> mWeightList;

public:
	EliMediator(std::vector<Theory*> theoryList, std::vector<double> weightList);
/*
	void mediateSituation() override;
	//std::vector<double> mediateDispositions() override;
	void mediateGatewayDisposition() override;
	void mediateNextDrinksDisposition() override;
	void mediateNonDrinkingActions() override;
*/
	void mediateSituation() override;
	//std::vector<double> mediateDispositions() override;
	void mediateGatewayDisposition() override;
	void mediateNextDrinksDisposition() override;
	void mediateNonDrinkingActions() override;
};

#endif /* ELI_MEDIATOR_H_ */