#include "Agent.h"
#include "Theory.h"
#include "TheoryMediator.h"
#include "globals.h"
#include "EliMediator.h"

/* Old Structure, needs altering */  // Make sure roles is first in theory list to get info on socialisation for autonomy in norms


EliMediator::EliMediator(std::vector<Theory*> theoryList, std::vector<double> weightList)
												: TheoryMediator(theoryList) {
													mWeightList = weightList;
												}

/*
EliMediator::EliMediator(std::vector<Theory*> theoryList, std::vector<double> weightList)
												{
													
													mTheoryList = theoryList;
													mWeightList = weightList;
												}
*/


void EliMediator::mediateSituation() {
	for (Theory* theory : mTheoryList) {
		theory->doSituation();				//doSituation could return value of socialisation modifier to be used for autonomy calc
	}
}
/*
std::vector<double> EliMediator::mediateDispositions() {
	std::vector<double> mediatedVector;
	for (int i = 0; i < MAX_DRINKS; ++i){
		mediatedVector.push_back(0);
	}

	for (int i = 0; i < mTheoryList.size(); ++i) {
		std::vector<double> tempVector = mTheoryList[i]->doDisposition(); // doDispositions not in theories - is in agent
		for (int j = 0; j < MAX_DRINKS; ++j) {
			mediatedVector[j] += mWeightList[i] * tempVector[j];
		}
	}

	return mediatedVector;
}
*/
// setDispositionByIndex used to alter agent's disposition vectors using mediated dispositions calculated from weighting of theories
void EliMediator::mediateGatewayDisposition() {
	/*
	std::vector<double> mediatedVector;
	for (int i = 0; i < MAX_DRINKS; ++i){		// Mediated vector of drinks dispositions
		mediatedVector.push_back(0);
	}
	*/
	
	/*
	for (int i = 0; i < mTheoryList.size(); ++i) {
		double tempDouble = 0; 		// Calc disposition from theory
		tempDouble = mTheoryList[i]->doGatewayDisposition();
		mediatedGatewayDouble += mWeightList[i]*tempDouble;		// Assign weighting of disposition and add to overall mediated disposition
	}
	*/
	double RoleDisp = mTheoryList[0]->doGatewayDisposition();
	double NormDisp = mTheoryList[1]->doGatewayDisposition();
	
	double mediatedDoubleFirstDrink;
	//GE-FIRST-DRINK
	
	//mediatedDoubleFirstDrink = mWeightList[0]*RoleDisp + mWeightList[1]*NormDisp;
	mpAgent->setDispositionByIndex(0, mediatedDoubleFirstDrink); // Add first Mediated Disposition to agent 
	/////// IMPORTANT ///// - mpAgent->setDispositionByIndex(j, mediatedVector[j]); // Add Mediated Dispositions to agent dispositions of size MAX_DRINKS

	//return mediatedVector; 

	//for (Theory* theory : mTheoryList) {
	//	theory->doGatewayDisposition();
	//}
}

void EliMediator::mediateNextDrinksDisposition() {
	/*
	std::vector<double> mediatedVector;
	for (int i = 0; i < MAX_DRINKS; ++i){
		mediatedVector.push_back(0);
	}
	*/
	
	/*
	for (int i = 0; i < mTheoryList.size(); ++i) {
		std::vector<double> tempVector = mTheoryList[i]->doNextDrinksDisposition();		// Calc disposition from theory
		for (int j = 1; j < MAX_DRINKS; ++j) {
			mediatedVector[j] += mWeightList[i]*tempVector[j];		// Assign weighting of disposition and add to vector elements
		}
	}
	for (int i = 1; i < MAX_DRINKS; ++i){
		mpAgent->setDispositionByIndex(i, mediatedVector[i]); // Add Mediated Dispositions to agent dispositions of size MAX_DRINKS
	}
	*/

	std::vector<double> RoleDispVect = mTheoryList[0]->doNextDrinksDisposition();
	std::vector<double> NormDispVect = mTheoryList[1]->doNextDrinksDisposition();
	for (int j = 1; j < MAX_DRINKS; ++j) {
			//mediatedDouble = mWeightList[0]*RoleDisp[j] + mWeightList[1]*NormDisp[j];		// Assign weighting of disposition and add to vector elements
			double RoleDisp = RoleDispVect[j];
			double NormDisp = NormDispVect[j];

			double mediatedDoubleNextDrink;
			//GE-NEXT-DRINK
			if (j==1) {
				//GE-2-DRINKS
			} else if (j>=2 && j<=3) {
				//GE-3-4-DRINKS
			} else if (j>=4 && j<=6) {
				//GE-5-7-DRINKS
			} else if (j>=7 && j<=10) {
				//GE-8-11-DRINKS
			} else {
				//GE-12-more-DRINKS
			}

			//mediatedDoubleNextDrink = mWeightList[0]*RoleDisp + mWeightList[1]*NormDisp;
			mpAgent->setDispositionByIndex(j, mediatedDoubleNextDrink);
	}

	// return mediatedVector;

	//for (Theory* theory : mTheoryList) {
	//	theory->doNextDrinksDisposition();
	//}
}

void EliMediator::mediateNonDrinkingActions(){
	for (Theory* theory : mTheoryList) {
		theory->doNonDrinkingActions();
	}
}