#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include <vector>
#include <stdio.h>


#include "Agent.h"
#include "TheoryMediator.h"
#include "Theory.h"
#include "StructuralEntity.h"
#include "MediatorForOneTheory.h"
#include "EliMediator.h"

// #include "NormModel.h"
#include "NormTheory.h"
#include "InjunctiveNormEntity.h"
#include "RegulatorInjunctiveBingePunishment.h"
#include "RegulatorInjunctiveRelaxation.h"
#include "DescriptiveNormEntity.h"
#include "NormGlobals.h"

// #include "RolesModel.h"
#include "RolesTheory.h"
#include "RolesStatisticsCollector.h"
#include "RolesEntity.h"
#include "RolesExogenousRegulator.h"

#include "HybridModel.h"
HybridModel::HybridModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm) :
					Model(propsFile, argc, argv, comm){
//////////////////////////////////////////////Roles///////////////////////////////////////////////////////////////
	std::string roleParamFilename = props->getProperty("role.parameter.file");

	ifstream roleParam(roleParamFilename);
	std::string row;
	while (!roleParam.eof()) {
        std::getline(roleParam, row);
        if (roleParam.bad() || roleParam.fail()) {
            break;
        }
        auto fields = readCSVRow(row);
		if (fields[0] == "roles.entity.rate.entry.marriage"){
			fields.erase(fields.begin());
			rateofEntryMarriageVector = fields;
		}
		if (fields[0] == "roles.entity.rate.entry.parenthood"){
			fields.erase(fields.begin());
			rateofEntryParenthoodVector = fields;
		}
		if (fields[0] == "roles.entity.rate.entry.employment"){
			fields.erase(fields.begin());
			rateofEntryEmploymentVector = fields;
		}
		if (fields[0] == "roles.entity.rate.exit.marriage"){
			fields.erase(fields.begin());
			rateofExitMarriageVector = fields;
		}
		if (fields[0] == "roles.entity.rate.exit.parenthood"){
			fields.erase(fields.begin());
			rateofExitParenthoodVector = fields;
		}
		if (fields[0] == "roles.entity.rate.exit.employment"){
			fields.erase(fields.begin());
			rateofExitEmploymentVector = fields;
		}
		if (fields[0] == "roles.entity.role.expectancy.marriage"){
			fields.erase(fields.begin());
			roleExpectancyMarriageVector = fields;
		}
		if (fields[0] == "roles.entity.role.expectancy.parenthood"){
			fields.erase(fields.begin());
			roleExpectancyParenthoodVector = fields;
		}
		if (fields[0] == "roles.entity.role.expectancy.employment"){
			fields.erase(fields.begin());
			roleExpectancyEmploymentVector = fields;
		}
	}

	//read in paramenters for situational mechanism (for calibration purpose)
	//repast::tokenize(props->getProperty("roles.theory.exponent.role.overload"), expRoleOverloadVector, ",");
	repast::tokenize(props->getProperty("role.load.beta1"), roleLoadBeta1Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.load.beta2"), roleLoadBeta2Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.load.beta3"), roleLoadBeta3Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.load.beta4"), roleLoadBeta4Vector, ",");//iteration2

	repast::tokenize(props->getProperty("role.drinking.opportunity.beta1"), roleDrinkingOpportunityBeta1Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.drinking.opportunity.beta2"), roleDrinkingOpportunityBeta2Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.drinking.opportunity.beta3"), roleDrinkingOpportunityBeta3Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.drinking.opportunity.beta3b"), roleDrinkingOpportunityBeta3bVector, ",");//iteration2
	repast::tokenize(props->getProperty("role.drinking.opportunity.beta4"), roleDrinkingOpportunityBeta4Vector, ",");//iteration2

	repast::tokenize(props->getProperty("role.strain.affecting.gateway.disposition.beta"), roleStrainAffectingGatewayDispositionBetaVector, ",");//iteration2
	repast::tokenize(props->getProperty("role.strain.affecting.next.drink.disposition.beta"), roleStrainAffectingNextDrinkDispositionBetaVector, ",");//iteration2

	//read in parameters for role disposition calculation
	//repast::tokenize(props->getProperty("roles.disposition.baseline.mean"), rolesDispositionBaselineMeanVector, ",");
	//repast::tokenize(props->getProperty("roles.disposition.baseline.sd"), rolesDispositionBaselineSDVector, ",");
	repast::tokenize(props->getProperty("role.socialisation.beta.more.roles"), roleSocialisationBetaMoreRolesVector, ",");
	repast::tokenize(props->getProperty("role.socialisation.beta.less.roles"), roleSocialisationBetaLessRolesVector, ",");
	mSocialisationSpeed = repast::strToDouble(props->getProperty("role.socialisation.speed"));
	//repast::tokenize(props->getProperty("roles.disposition.beta.disposition.affected.by.role.strain.male"), rolesDispositionBetaAffectedByRoleStrainMaleVector, ",");
	//repast::tokenize(props->getProperty("roles.disposition.beta.disposition.affected.by.role.strain.female"), rolesDispositionBetaAffectedByRoleStrainFemaleVector, ",");

	//read in parameters for role opportunity calculation
	repast::tokenize(props->getProperty("roles.opportunity.baseline.opportunity"), baselineOpportunityVector, ",");
	mBalanceOpportunity = repast::strToDouble(props->getProperty("roles.opportunity.balance.opportunity"));

	//socialisation on-off
	ROLES_SOCIALISATION_ON = repast::strToInt(props->getProperty("role.socialisation.on"));

	//read parameters from file and store in a table for init agents function (used by Model as well)
	int rank = repast::RepastProcess::instance()->rank();
	std::string fileNameRankProperty = "file.rank" + std::to_string(rank);
	std::string fileNameRank = props->getProperty(fileNameRankProperty);
	readRankFileForTheory(fileNameRank);

	//regulators that affect the structural entities
	std::vector<Regulator*> regulatorList;
	regulatorList.push_back( new RolesExogenousRegulator(&context) );

	//power of each regulator (sum = 1)
	std::vector<double> powerList;
	//powerList.push_back(1.0);
	int rolesEntityInterval = repast::strToInt(props->getProperty("transformational.interval.roles.entity"));

	//create a structural entity (for transformational mechanisms)
	StructuralEntity *roleEntity = new RolesEntity(regulatorList, \
													powerList, \
													rolesEntityInterval, \
													roleExpectancyMarriageVector, \
													roleExpectancyParenthoodVector, \
													roleExpectancyEmploymentVector, \
													&context
													);
	structuralEntityList.push_back(roleEntity);
////////////////////////////////////////////////Norms/////////////////////////////////////////////////////////////
	// Read parameters for injunctive norms and payoff adjustment from model.props file
	std::string strInjThreshold = props->getProperty("norms.injunctive.threshold");
	std::string strInjProportion = props->getProperty("norms.injunctive.proportion");
	std::string strInjRelaxationGammaAdjustment = props->getProperty("norms.injunctive.relaxation.gamma.adjustment");
	std::string strInjRelaxationLambdaAdjustment = props->getProperty("norms.injunctive.relaxation.lambda.adjustment");
	std::string strInjPunishmentGammaAdjustment = props->getProperty("norms.injunctive.punishment.gamma.adjustment");
	std::string strInjPunishmentLambdaAdjustment = props->getProperty("norms.injunctive.punishment.lambda.adjustment");
	std::string strNDaysDes = props->getProperty("norms.n.days.descriptive");
	std::string strCompDaysPunish = props->getProperty("norms.com.days.punish");
	std::string strCompDaysRelax = props->getProperty("norms.com.days.relax");
	std::string strPerceptionBias = props->getProperty("bias.factor");
	std::string strDiscountMale = props->getProperty("discount.male");
	std::string strDiscountFemale = props->getProperty("discount.female");
	std::string strDesireMultipilerAbstainer = props->getProperty("desire.multiplier.abstainer");
	std::string strDesireMultipilerDrinker = props->getProperty("desire.multiplier.drinker");

	if (!strInjThreshold.empty()) { INJUNCTIVE_THRESHOLD = repast::strToInt(strInjThreshold); }
	if (!strInjProportion.empty()) { INJUNCTIVE_PROPORTION = repast::strToDouble(strInjProportion); }
	if (!strInjRelaxationGammaAdjustment.empty()) { INJ_RELAXATION_GAMMA_ADJUSTMENT = repast::strToDouble(strInjRelaxationGammaAdjustment); }
	if (!strInjRelaxationLambdaAdjustment.empty()) { INJ_RELAXATION_LAMBDA_ADJUSTMENT = repast::strToDouble(strInjRelaxationLambdaAdjustment); }
	if (!strInjPunishmentGammaAdjustment.empty()) { INJ_PUNISHMENT_GAMMA_ADJUSTMENT = repast::strToDouble(strInjPunishmentGammaAdjustment); }
	if (!strInjPunishmentLambdaAdjustment.empty()) { INJ_PUNISHMENT_LAMBDA_ADJUSTMENT = repast::strToDouble(strInjPunishmentLambdaAdjustment); }
	if (!strNDaysDes.empty()) {N_DAYS_DESCRIPTIVE = repast::strToDouble(strNDaysDes); }
	if (!strCompDaysPunish.empty()) {COMP_DAYS_PUNISH = repast::strToDouble(strCompDaysPunish); }
	if (!strCompDaysRelax.empty()) {COMP_DAYS_RELAX = repast::strToDouble(strCompDaysRelax); }
	if (!strPerceptionBias.empty()) {PERCEPTION_BIAS = repast::strToDouble(strPerceptionBias);}
	if (!strDiscountMale.empty()) {DISCOUNT_MALE = repast::strToDouble(strDiscountMale);}
	if (!strDiscountFemale.empty()) {DISCOUNT_FEMALE = repast::strToDouble(strDiscountFemale);}
	if (!strDesireMultipilerAbstainer.empty()) {DESIRE_MULTIPLIER_ABSTAINER = repast::strToDouble(strDesireMultipilerAbstainer);}
	if (!strDesireMultipilerDrinker.empty()) {DESIRE_MULTIPLIER_DRINKER = repast::strToDouble(strDesireMultipilerDrinker);}
	/*
	//read parameters from file and store in a table for init agents function (used by Model as well)
	int rank = repast::RepastProcess::instance()->rank();
	std::string rankFileNameProperty = "file.rank" + std::to_string(rank);
	std::string rankFileName = props->getProperty(rankFileNameProperty);
	readRankFileForTheory(rankFileName);
	*/
	//regulators that affect the structural entities
	std::vector<Regulator*> regulatorListInj;
	mpRegPunishment = new RegulatorInjunctiveBingePunishment(&context);
	mpRegRelaxation = new RegulatorInjunctiveRelaxation(&context);
	regulatorListInj.push_back(mpRegPunishment);
	regulatorListInj.push_back(mpRegRelaxation);

	//power of each regulator (sum = 1)
	std::vector<double> powerListInj;
	powerListInj.push_back(0.5);
	powerListInj.push_back(0.5);

	//dummy list for Des Norm
	std::vector<Regulator*> regulatorListDes;
	std::vector<double> powerListDes;

	//create a structural entities
	std::vector<std::string> injNormGate;
	repast::tokenize(props->getProperty("norms.injunctive.gate"), injNormGate, ",");
	std::vector<std::string> injNormGamma;
	repast::tokenize(props->getProperty("norms.injunctive.gamma"), injNormGamma, ",");
	std::vector<std::string> injNormLambda;
	repast::tokenize(props->getProperty("norms.injunctive.lambda"), injNormLambda, ",");

	mIntervalDesNorm = repast::strToInt(props->getProperty("transformational.interval.descriptive.norm"));
	mIntervalPunish = repast::strToInt(props->getProperty("transformational.interval.punish"));
	mIntervalRelax = repast::strToInt(props->getProperty("transformational.interval.relax"));

	InjunctiveNormEntity *pInjNorm = new InjunctiveNormEntity(regulatorListInj, powerListInj, mIntervalPunish, mIntervalRelax, injNormGate, injNormGamma, injNormLambda);
	DescriptiveNormEntity *pDesNorm = new DescriptiveNormEntity(regulatorListDes, powerListDes, mIntervalDesNorm, &context);
	structuralEntityList.push_back(pInjNorm);
	structuralEntityList.push_back(pDesNorm);

	//Norm outputs (1 core)
	if (AGENT_LEVEL_OUTPUT) {
		std::string annualFileName = addUniqueSuffix("outputs/annual_norm_output.csv");
		annualNormOutput.open(annualFileName);
		annualNormOutput << "Year,";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "DesPrevelance" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "DesQuant" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "InjGate" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "InjGamma" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "InjLambda" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "IntervalPunishment" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "IntervalRelaxation" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "PopulationCount" << i << j << ",";
		annualNormOutput << std::endl;
	}
	// Get weighting for roles from props file
	std::string strWeightRoles = props->getProperty("weight.roles");
	if (!strWeightRoles.empty()) { WEIGHT_ROLES = repast::strToDouble(strWeightRoles); }
}

HybridModel::~HybridModel() {}

void HybridModel::readRankFileForTheory(std::string rankFileName) { // Mainly the same as funtion in RolesModel, added norms.autonomy
#ifdef DEBUG
		std::cout << "Reading the file: " << rankFileName <<std::endl;
#endif
	ifstream myfile(rankFileName);
	if (myfile.is_open()) {
		//read the csv file
		infoTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << rankFileName << std::endl;
	}

	//find index of theory-specific variables, both roles and norms
	std::vector<string> headerLine = infoTable.front();
	for (int i = 0; i < headerLine.size(); ++i) {
		if (headerLine[i]=="roles.marital.involvement.level") mIndexMaritalInvolvement = i;
		if (headerLine[i]=="roles.parenthood.involvement.level") mIndexParenthoodInvolvement = i;
		if (headerLine[i]=="roles.employment.involvement.level") mIndexEmploymentInvolvement = i;
		if (headerLine[i]=="roles.ability.of.coping.role.strain") mIndexAbilityCopingRoleStrain = i;
		if (headerLine[i]=="roles.skill") mIndexRoleSkill = i;
		//if (headerLine[i]=="role.disposition.baseline.mean") mIndexDispositionBaselineMean = i;
		//if (headerLine[i]=="role.disposition.baseline.sd") mIndexDispositionBaselineSD = i;
		if (headerLine[i]=="roles.gateway.disposition.initialization") mIndexGatewayDispositionInitialization = i;
		if (headerLine[i]=="norms.autonomy") mIndexAutonomy = i;
	}
}

void HybridModel::initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info){
	
	repast::AgentId  agentId = agent->getId();
	int id = agentId.id();

//Roles initialisation
	//obtaining corresponding value for agent
	//int educationStatus = repast::strToInt(educationStatusVector[id]);//not considering in Iteration 1
	//int levelofInvolvementEducation = repast::strToInt(levelofInvolvementEducationVector[id]);//not considering in Iteration 1
	double levelofInvolvementMarriage = repast::strToDouble(info[mIndexMaritalInvolvement]);
	double levelofInvolvementParenthood = repast::strToDouble(info[mIndexParenthoodInvolvement]);
	double levelofInvolvementEmployment = repast::strToDouble(info[mIndexEmploymentInvolvement]);
	double abilityofCopingRoleStrain = repast::strToDouble(info[mIndexAbilityCopingRoleStrain]);
	double roleSkill = repast::strToDouble(info[mIndexRoleSkill]);
	double gatewayDispositionInitialization = repast::strToDouble(info[mIndexGatewayDispositionInitialization]);
	//int expRoleOverload = repast::strToInt(expRoleOverloadVector[0]);
	//role load
	double roleLoadBeta1 = repast::strToDouble(roleLoadBeta1Vector[0]);//iteration2
	double roleLoadBeta2 = repast::strToDouble(roleLoadBeta2Vector[0]);//iteration2
	double roleLoadBeta3 = repast::strToDouble(roleLoadBeta3Vector[0]);//iteration2
	double roleLoadBeta4 = repast::strToDouble(roleLoadBeta4Vector[0]);//iteration2
	//role drinking opportunity
	double roleDrinkingOpportunityBeta1 = repast::strToDouble(roleDrinkingOpportunityBeta1Vector[0]);//iteration2
	double roleDrinkingOpportunityBeta2 = repast::strToDouble(roleDrinkingOpportunityBeta2Vector[0]);//iteration2
	double roleDrinkingOpportunityBeta3 = repast::strToDouble(roleDrinkingOpportunityBeta3Vector[0]);//iteration2
	double roleDrinkingOpportunityBeta3b = repast::strToDouble(roleDrinkingOpportunityBeta3bVector[0]);//iteration2
	double roleDrinkingOpportunityBeta4 = repast::strToDouble(roleDrinkingOpportunityBeta4Vector[0]);//iteration2

	//role gateway disposition
	double roleStrainAffectingGatewayDispositionBeta = repast::strToDouble(roleStrainAffectingGatewayDispositionBetaVector[0]);//iteration2
	double roleStrainAffectingNextDrinkDispositionBeta = repast::strToDouble(roleStrainAffectingNextDrinkDispositionBetaVector[0]);//iteration2

	//double rolesDispositionBaselineMean = repast::strToDouble(infoTable[id][mIndexDispositionBaselineMean]);
	//double rolesDispositionBaselineSD = repast::strToDouble(infoTable[id][mIndexDispositionBaselineSD]);
	double rolesSocialisationBetaMoreRoles = repast::strToDouble(roleSocialisationBetaMoreRolesVector[0]);
	double rolesSocialisationBetaLessRoles = repast::strToDouble(roleSocialisationBetaLessRolesVector[0]);
	//double rolesDispositionBetaAffectedByRoleStrainMale = repast::strToDouble(rolesDispositionBetaAffectedByRoleStrainMaleVector[0]);
	//double rolesDispositionBetaAffectedByRoleStrainFemale = repast::strToDouble(rolesDispositionBetaAffectedByRoleStrainFemaleVector[0]);
	double baselineOpportunity = repast::strToDouble(baselineOpportunityVector[0]);
	//int rolesDiscontinuity = repast::strToInt(rolesDiscontinuityVector[id]);//not considering in Iteration 1
	//int roleStrain = repast::strToInt(roleStrainVector[id]);

	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList;

	theoryList.push_back( new RolesTheory(&context, \
									(RolesEntity *) structuralEntityList[0], \
									//educationStatus, //not considering in Iteration 1
									//levelofInvolvementEducation, //not considering in Iteration 1
									levelofInvolvementMarriage, \
									levelofInvolvementParenthood, \
									levelofInvolvementEmployment, \
									abilityofCopingRoleStrain, \
									//expRoleOverload,
									roleSkill, \
									gatewayDispositionInitialization, \
									//rolesDispositionBaselineMean,
									//rolesDispositionBaselineSD,
									roleLoadBeta1, \
									roleLoadBeta2, \
									roleLoadBeta3, \
									roleLoadBeta4, \
									roleDrinkingOpportunityBeta1, \
									roleDrinkingOpportunityBeta2, \
									roleDrinkingOpportunityBeta3, \
									roleDrinkingOpportunityBeta3b, \
									roleDrinkingOpportunityBeta4, \
									roleStrainAffectingGatewayDispositionBeta, \
									roleStrainAffectingNextDrinkDispositionBeta, \
									rolesSocialisationBetaMoreRoles, \
									rolesSocialisationBetaLessRoles, \
									//rolesDispositionBetaAffectedByRoleStrainMale,
									//rolesDispositionBetaAffectedByRoleStrainFemale,
									baselineOpportunity, \
									mBalanceOpportunity, \
									mSocialisationSpeed
									//rolesDiscontinuity, //not considering in Iteration 1
									) );

//Norms initialisation
	double autonomy = repast::strToDouble(info[mIndexAutonomy]);
	
	//NormTheory* theory = new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[0],
	//				(DescriptiveNormEntity *) structuralEntityList[1], autonomy);
	
	NormTheory* Ntheory = new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[1],
				(DescriptiveNormEntity *) structuralEntityList[2]);
	theoryList.push_back(Ntheory);
	//theoryList.push_back(new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[1], // Structural entity list starts with role entity
	//			(DescriptiveNormEntity *) structuralEntityList[2], autonomy));
	
	//Set weights for theories
	std::vector<double> weightList;
	weightList.push_back(WEIGHT_ROLES);
	weightList.push_back(1 - WEIGHT_ROLES);
	//create theory(ies) and a mediator for each agent
	//TheoryMediator *mediator = new EliMediator(theoryList,weightList);
	EliMediator *mediator = new EliMediator(theoryList,weightList);
	
	//link agent with the mediator
	agent->setMediator(mediator);
	Ntheory->initDesires();
}

void HybridModel::initMediatorAndTheoryWithRandomParameters(Agent *agent) {
	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList;
	RolesTheory* Rtheory = new RolesTheory(&context, (RolesEntity *) structuralEntityList[0]);
	theoryList.push_back( Rtheory);
	NormTheory* Ntheory = new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[1],
				(DescriptiveNormEntity *) structuralEntityList[2]);
	theoryList.push_back(Ntheory);

	//Set weights for theories
	std::vector<double> weightList;
	weightList.push_back(WEIGHT_ROLES);
	weightList.push_back(1 - WEIGHT_ROLES);
	//create theory(ies) and a mediator for each agent
	//TheoryMediator *mediator = new EliMediator(theoryList,weightList);
	EliMediator *mediator = new EliMediator(theoryList,weightList);
	
	//link agent with the mediator
	agent->setMediator(mediator);
	Ntheory->initDesires();
}

void HybridModel::initForTheory(repast::ScheduleRunner& runner) {
	RolesEntity *pRoleEntity = (RolesEntity *) structuralEntityList[0];
	pRoleEntity->doTransformation();
	//update avg drinking values before 1st situational mechanism
	((DescriptiveNormEntity *) structuralEntityList[2])->updateDescriptiveGroupDrinking();
}

std::string HybridModel::addUniqueSuffix(std::string fileName){

	if (!boost::filesystem::exists(fileName)){ //check if the file doesn't exist.
		return fileName;				       //if not, return the filename as is.

	}else{
		//I will assume that if the file basename doesn't exists, none of the numbered versions do either.
		//If numbered versions exist while the base fileName file doens't exist, this will overwrite them.
		int i = 1;
		std::string noExtensionFileName = fileName;
		for (int count = 0; count < 4; ++count){
			noExtensionFileName.pop_back();
		}
		std::string testFileName = noExtensionFileName + "_" + to_string(i) + ".csv";
		while (boost::filesystem::exists(testFileName))
		{
			++i;
			testFileName = noExtensionFileName + "_" + to_string(i) + ".csv";
		}
		return testFileName;
	}
}

void HybridModel::writeAnnualAgentDataToFile() {
	if (AGENT_LEVEL_OUTPUT && annualNormOutput.is_open()){
		int size = P_REFERENCE_GROUP->size();

		annualNormOutput << simYear << ",";
		DescriptiveNormEntity* pDesNormEntity = (DescriptiveNormEntity *) structuralEntityList[1];
		for (int i=0; i<size; ++i)
			annualNormOutput << pDesNormEntity->getAvgIsDrinking(i) << ",";
		for (int i=0; i<size; ++i)
			annualNormOutput << pDesNormEntity->getAvgNumberDrinks(i) << ",";

		InjunctiveNormEntity* pInjNormEntity = (InjunctiveNormEntity *) structuralEntityList[0];
		for (int i=0; i<size; ++i)
			annualNormOutput << pInjNormEntity->getInjNormGate(i) << ",";
		for (int i=0; i<size; ++i)
			annualNormOutput << pInjNormEntity->getInjNormGamma(i) << ",";
		for (int i=0; i<size; ++i)
			annualNormOutput << pInjNormEntity->getInjNormLambda(i) << ",";

		for (int i=0; i<size; ++i)
			annualNormOutput << mpRegPunishment->mTransformationalTriggerCount[i]/(365.0/mIntervalPunish) << ",";
		for (int i=0; i<size; ++i)
			annualNormOutput << mpRegRelaxation->mTransformationalTriggerCount[i]/(365.0/mIntervalRelax) << ",";

		int count[size] = {0};
		repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
		repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
		while (iter != iterEnd) {
			count[P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup())] += 1;
			iter++;
		}
		
		for (int i=0; i<size; ++i) {
			annualNormOutput << count[i] << ",";
		}

		annualNormOutput << std::endl;

		pInjNormEntity->mTransformationalTriggerCount = 0;
		pDesNormEntity->mTransformationalTriggerCount = 0;
		mpRegPunishment->resetCount();
		mpRegRelaxation->resetCount();
	} else {
		std::cerr << "Error: Can't write to annual_norm_output.csv, file not open." << std::endl;
	}
}



