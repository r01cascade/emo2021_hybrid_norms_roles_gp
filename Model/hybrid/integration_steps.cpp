#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "Model.h"
#include "globals.h"
#include "StatisticsCollector.h"
#include "Log.h"


/* Dynamic Autonomy */
// in norms/NormModel.cpp
double NormModel::initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info) { // altering function to give autonomy as output
	double autonomy = repast::strToDouble(info[mIndexAutonomy]);

	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList;
	NormTheory* theory = new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[0],
				(DescriptiveNormEntity *) structuralEntityList[1], autonomy);
	theoryList.push_back(theory);
	TheoryMediator *mediator = new MediatorForOneTheory(theoryList);

	//link agent with the mediator
	agent->setMediator(mediator);
	theory->initDesires();
	return autonomy;
}
// in hybrid/HybridModel.cpp ??
double HybridModel::dynamicAutonomy(double calRoleSocialization *autonomyModifier, double autonomy){
	// double autonomy = repast::strToDouble(info[mIndexAutonomy])
	// double newAutonomyArray = new double[countOfAgents]; 
	modifier = autonomyModifier
	if(autonomy >= 0.5) // Decrease in autonomy when days of socialisation is less than socialisation speed
	{
		newAutonomy = autonomy + (autonomy)*(0.5-modifier);
	}
	else if(autonomy < 0.5) // Increase in autonomy when days of socialisation is more than socialisation speed
	{

		newAutonomy = autonomy + (1 - autonomy)*(0.5-modifier);
	}
	else
	{
		std::cerr << "Autonomy is not in (0,1) range" << std::endl;
	}
	countOfAgents = repast::strToInt(props->getProperty("count.of.agents")); // Used for loop of changing each agents' autonomy

	// Write new values of autonomy in array
	//for (int i = 0; i < countOfAgents; ++i){
	//	newAutonomyArray[i] = newAutonomy;
	//}
	return newAutonomy;
}

// Copy new autonomy to agents database
/*
void HybridModel::updateRankFileForTheory(std::string agentsData, double newAutonomy, double mIndexAutonomy) {
#ifdef DEBUG
		std::cout << "Reading the file: " << rankFileName <<std::endl;
#endif
std::fstream agentsData;
agentsData.open("agents.rank1.csv"); 
	if (agentsData.is_open()) {
		//read the csv file
		infoTable = readCSV(agentsData);
	} else {
		std::cerr << "Unable to open file: " << agentsData << std::endl;
	}

	//find index of theory-specific variables and replace them with new values
	mIndexAutonomy = -1;
	std::vector<string> headerLine = infoTable.front();
	//for (int j = 0; j < countOfAgents; j++){
		for (int i = 0; i < headerLine.size(); ++i) {
			if ( headerLine[i]=="norms.autonomy" )
				//agentsData << *(newAutonomyArray + j);
				agentsData << newAutonomy;
				}
	//}
}
*/
void HybridModel::updateRankFileForTheory(std::string agentsData, double newAutonomy)
{
	std::fstream agentsData;
	agentsData.open("dynamic.autonomies.csv")

}

// in roles/RolesTheory.cpp
double RolesTheory::calRoleSocialization(){		// altering function to give modifier as output

	double dayBar;
	double modifier;
	double dispositionDiff;

	if (mpAgent->getRoleChangedTransient()){
		mDaysOfSocialisation = 1;
		mStateOfRoleChange = mpAgent->getRoleChangeStatus();
		mDispositionVectorOld = mDispositionVector;
		mStartSocialising = true;
	}

	//end socialisation after a year
	if (mDaysOfSocialisation == 365 && mStartSocialising) mStartSocialising = false;

	// dayBar = (mDaysOfSocialisation - 365 / 2) / (365 / mBetaSocialisationSpeed);
	dayBar = (mDaysOfSocialisation - mSocialisationSpeed) / 365;
	modifier = exp(dayBar)/(1+exp(dayBar));

	//for more roles
	if (mStateOfRoleChange == 1 && mStartSocialising){

		for (int i=0; i<MAX_DRINKS; ++i) {

			dispositionDiff = mDispositionVectorOld[i] * (1 + mRolesSocialisationBetaMoreRoles) - mDispositionVectorOld[i];

			mDispositionVector[i] = mDispositionVectorOld[i] + dispositionDiff * modifier;
		}

		/*repast::AgentId AgentID = mpAgent->getId();
		if (AgentID.id() == 201)
			std::cout<<"mDaysOfSocialisation="<<mDaysOfSocialisation<<" mDispositionVector[1]="<<mDispositionVector[1]<<std::endl;
		*/
		mDaysOfSocialisation++;
	}
	//for less roles
	else if (mStateOfRoleChange == -1 && mStartSocialising){

		for (int i=1; i<MAX_DRINKS; ++i) {

			dispositionDiff = mDispositionVectorOld[i] * (1 + mRolesSocialisationBetaLessRoles) - mDispositionVectorOld[i];

			mDispositionVector[i] = mDispositionVectorOld[i] + dispositionDiff * modifier;

		}

		/*repast::AgentId AgentID = mpAgent->getId();
		if (AgentID.id() == 201)
			std::cout<<"mDaysOfSocialisation="<<mDaysOfSocialisation<<" mDispositionVector[1]="<<mDispositionVector[1]<<std::endl;
		*/
		mDaysOfSocialisation++;
	}
	return modifier;
 }



/* Expanding of characteristic groups */
// in core/globals
	NUM_ROLES_AMMOUNT = 4; // Number of possible ammounts of roles
// in norm/NormReferenceGroup.cpp
   	mNumberOfReferenceGroups = NUM_SEX*NUM_AGE_GROUPS*NUM_ROLES_AMMOUNT;

   	NormReferenceGroup::NormReferenceGroup() {
	// for loop, init all ref groups
	mNumberOfReferenceGroups = NUM_SEX*NUM_AGE_GROUPS*NUM_ROLES_AMMOUNT;
	mReferenceGroups = new std::tuple<int, int, int>[mNumberOfReferenceGroups];
	int index = 0;
	for (int i=0; i<NUM_SEX; ++i) {
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			for (int k=0; k<NUM_ROLES_AMMOUNT; k++) {
				mReferenceGroups[index] = std::make_tuple(i, j, k);
				index++;
				}
			}
		}
	}

	int NormReferenceGroup::getId(int sex, int ageGroup, int roleNum) {
	std::tuple<int, int, int> refGroup = std::make_tuple(sex, ageGroup, roleNum);
	for (int i=0; i<mNumberOfReferenceGroups; i++) {
		if (mReferenceGroups[i] == refGroup)
			return i;
	}

	std::cerr << "Error: Do not find any reference group for [" << sex << " " << ageGroup << " " roleNum "]." << std::endl;
	return -1; //do not find any matching reference group
}

	int NormReferenceGroup::compare(int id1, int id2) {
	// declare local integer as counter
	int numberShared = 0;

	// compare and count number of shared attributes
	std::tuple<int, int, int> refGroup1 = mReferenceGroups[id1];
	std::tuple<int, int, int> refGroup2 = mReferenceGroups[id2];

	// sum the difference (note: cannot loop the index for tuple)
	numberShared += int (std::get<0>(refGroup1) == std::get<0>(refGroup2));
	numberShared += int (std::get<1>(refGroup1) == std::get<1>(refGroup2));
	numberShared += int (std::get<2>(refGroup1) == std::get<2>(refGroup2));
	// return number of shared attributes
	return numberShared;
	}

// in norm/NormReferenceGroup.h
	private:
	int mNumberOfReferenceGroups;
	std::tuple<int, int, int> *mReferenceGroups;

	public:
	NormReferenceGroup();
	~NormReferenceGroup();

	int size();

	int getId(int sex, int ageGroup, int roleNum);
	int compare(int id1, int id2);

// in core/Agent.cpp
   	int Agent::getNumberOfRoles() {
  		int mNumberOfRoles = 0;
    	if (mMaritalStatus == 1)
    	{
    		mNumberOfRoles++;
    	}
    	if (mParenthoodStatus == 1)
    	{
    		mNumberOfRoles++;
    	}
   		if (mEmploymentStatus == 1)
   		{
   			mNumberOfRoles++;
   		}
    	return mNumberOfRoles;
   	}
// in core/Agent.h
    public:
    int getNumberOfRoles();

// in norms/NormTheory.cpp
    void NormTheory::doSituation() {
	mReferenceGroupId = P_REFERENCE_GROUP->getId(mpAgent->getSex(), mpAgent->findAgeGroup());
	calcDescriptiveNorm();
	}

// in norms/NormModel.cpp writeAnnualAgentDataToFile()
	int count[size] = {0};
		repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
		repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
		while (iter != iterEnd) {
			count[P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup())] += 1;
			iter++;
		}
