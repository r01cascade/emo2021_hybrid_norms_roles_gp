/* Model.cpp */

#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include <istream>
#include <string>
#include <cmath>
#include <unordered_map>

#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"

#include "Model.h"
#include "globals.h"
#include "StatisticsCollector.h"

#include "Log.h"


AgentPackageProvider::AgentPackageProvider(repast::SharedContext<Agent>* agentPtr): agents(agentPtr){ }

void AgentPackageProvider::providePackage(Agent * agent, std::vector<AgentPackage>& out){
    repast::AgentId id = agent->getId();
    AgentPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank(), agent->getAge(), agent->getSex(), agent->isDrinkingToday(),
    	                 agent->getNumberDrinksToday(), agent->getDrinkFrequencyLevel(), agent->getPastYearDrinks());
    out.push_back(package);
}

void AgentPackageProvider::provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out){
    std::vector<repast::AgentId> ids = req.requestedAgents();
    for(size_t i = 0; i < ids.size(); i++){
        providePackage(agents->getAgent(ids[i]), out);
    }
}

AgentPackageReceiver::AgentPackageReceiver(repast::SharedContext<Agent>* agentPtr): agents(agentPtr){}

Agent * AgentPackageReceiver::createAgent(AgentPackage package){
    repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
    return new Agent(id); // When agent moves to other process he/she only takes ID; all other properties will be reset
}

void AgentPackageReceiver::updateAgent(AgentPackage package){
    repast::AgentId id(package.id, package.rank, package.type);
    Agent * agent = agents->getAgent(id);
    agent->set(package.currentRank, package.age, package.sex, package.isDrinkingToday, package.numberDrinksToday,
    		   package.drinkFrequencyLevel, package.pastYearDrinks);
}


Model::Model(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm){
	props = new repast::Properties(propsFile, argc, argv, comm);
	stopAt = repast::strToInt(props->getProperty("stop.at"));
	countOfAgents = repast::strToInt(props->getProperty("count.of.agents"));
	startYear = repast::strToInt(props->getProperty("start.year"));
	simYear = 0;
	simDay = 0;

	provider = new AgentPackageProvider(&context);
	receiver = new AgentPackageReceiver(&context);
	startTime = std::chrono::steady_clock::now();

	//read flags and custom run settings
	AGENT_LEVEL_OUTPUT = repast::strToInt(props->getProperty("agent.level.output"));
	SPAWNING_ON = repast::strToInt(props->getProperty("spawning.on"));
	ROLE_TRANSITION_ON = repast::strToInt(props->getProperty("role.transition.on"));
	PRINT_POPULATION_STAT_YEARLY = repast::strToInt(props->getProperty("print.population.stat.yearly"));
	SINGLE_RUN_LIMIT_MIN = repast::strToInt(props->getProperty("single.run.limit.minute"));
	FAIL_FAST = repast::strToInt(props->getProperty("fail.fast.on"));

	//Read intervals for 3 mechanisms
	ACTION_MECHANISM_INTERVAL_TICK = repast::strToInt(props->getProperty("action.interval"));
	SITUATIONAL_MECHANISM_INTERVAL_TICK = repast::strToInt(props->getProperty("situational.interval"));

	//read in ERFC lookup table
	std::string lookupFilename = props->getProperty("compressed.lookup.file");
	readMeanSDLookupTable(lookupFilename);

	//read role transition probability
	if (ROLE_TRANSITION_ON) {
		std::string tpFilename = props->getProperty("transition.probability.file");
		readRoleTPTable(tpFilename);
	}

	//read death rates
	std::string deathRateFilename = props->getProperty("death.rates.file");
	readDeathRateTable(deathRateFilename);

	//read migration out rates
	std::string migrationOutFilename = props->getProperty("migration.out.rates.file");
	readMigrationOutTable(migrationOutFilename);

	//custom-logging flag
	CUSTOM_LOG = props->getProperty("custom.log").empty() ? 0 : repast::strToInt(props->getProperty("custom.log"));
	if (CUSTOM_LOG) {
		Logger("Starting...");
	}

	//full path of annual data file
	ANNUAL_DATA_FILE = props->getProperty("annual.data.file").empty() ? "./outputs/annual_data.csv" : props->getProperty("annual.data.file");
}


void Model::readRoleTPTable(std::string tpFilename) {

	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(tpFilename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << tpFilename << std::endl;
	}

	//find index on header line.
	std::vector<string> headerLine = localTable.front();
	localTable.pop();

	int indexYear = findIndexInHeader("year", headerLine);
	int indexSex = findIndexInHeader("sex", headerLine);
	int indexAge = findIndexInHeader("age", headerLine);
	int indexCurrentM = findIndexInHeader("currentMarriage", headerLine);
	int indexCurrentP = findIndexInHeader("currentParenthood", headerLine);
	int indexCurrentE = findIndexInHeader("currentEmployment", headerLine);
	int indexNextM = findIndexInHeader("nextMarriage", headerLine);
	int indexNextP = findIndexInHeader("nextParenthood", headerLine);
	int indexNextE = findIndexInHeader("nextEmployment", headerLine);
	int indexTP = findIndexInHeader("tp", headerLine);

	//read in variables and create map stored in globals.
	std::string localKey;
	double localTP;

	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<string> localMap = localTable.front();
		localTable.pop();
	
		//read the variables from a row of localTable and make hashkey
		localKey = makeRoleTPHashKey(localMap[indexYear], localMap[indexSex], localMap[indexAge], \
								  localMap[indexCurrentM], localMap[indexCurrentP], localMap[indexCurrentE], \
								  localMap[indexNextM], localMap[indexNextP], localMap[indexNextE]);
		localTP = repast::strToDouble(localMap[indexTP]);
		//create a key/pair map
		roleTransitionProbTable.insert({localKey,localTP});
		
	}
}

std::string Model::makeRoleTPHashKey(std::string year, std::string sex, std::string age, \
							      std::string currentMarriage, std::string currentParenthood, std::string currentEmployment, \
							      std::string nextMarriage, std::string nextParenthood, std::string nextEmployment) {

	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = year + sex + age + currentMarriage + currentParenthood + currentEmployment + nextMarriage + nextParenthood + nextEmployment;
	
	return std::to_string(tp_hash(hash_index));
}

void Model::readMeanSDLookupTable(std::string lookupFilename) {

	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(lookupFilename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open lookup file: " << lookupFilename << std::endl;
	}

	//find index on header line.
	std::vector<string> headerLine = localTable.front();
	localTable.pop();

	int indexKey = findIndexInHeader("hashkey", headerLine);
	int indexMean = findIndexInHeader("ERFC.mean", headerLine);
	int indexSd = findIndexInHeader("ERFC.SD", headerLine);
	
	//read in variables and create map stored in globals.
	int localKey;
	double localMean;
	double localSd;
	std::pair<double, double> localMeanSd;
	
	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<string> localMap = localTable.front();
		localTable.pop();

		//read the variables from a row of localTable
		localKey = repast::strToInt(localMap[indexKey]);
		localMean = repast::strToDouble(localMap[indexMean]);
		localSd = repast::strToDouble(localMap[indexSd]);
		localMeanSd = std::make_pair(localMean, localSd);
		//create a key/pair map from row.
		
		MEAN_SD_LOOKUP_TABLE.insert(std::make_pair(localKey, localMeanSd));
	
	}

}

Model::~Model(){
	if (CUSTOM_LOG) {
		Logger("Maximum memory used: " + std::to_string(getMaxMemoryUsed()/1024) + "MB");//logging maximum memory use
	}
	
	delete props;
	delete provider;
	delete receiver;
	delete annualValues;
	for (std::vector<StructuralEntity*>::iterator it=structuralEntityList.begin(); it!= structuralEntityList.end(); ++it)
		delete (*it);

	if (CUSTOM_LOG) {
		auto elapsed = std::chrono::duration_cast<std::chrono::seconds>( std::chrono::steady_clock::now() - startTime );
		Logger("End - runtime: " + std::to_string(elapsed.count()) + "sec");//logging runtime
	}
}

int Model::findIndexInHeader(std::string string, std::vector<std::string> headerLine) {
	std::vector<std::string>::iterator it = std::find(headerLine.begin(), headerLine.end(), string);
	if (it == headerLine.end()) {
		std::cout << "Index Not Found: " << string << std::endl;
		return -1;
	}

	// Found: return index of element from iterator
	return std::distance(headerLine.begin(), it);
}

// Generating agents, theories, structural entities
void Model::initAgents(){
	double monthlyDrinksSDPct = repast::strToDouble(props->getProperty("microsim.init.monthly.sd.pct"));

	//find index from the header line, ASSUMING the infoTable was already be read
	std::vector<string> headerLine = infoTable.front();
	infoTable.pop();

	mIndexId = findIndexInHeader("microsim.init.id", headerLine);
	mIndexSex = findIndexInHeader("microsim.init.sex", headerLine);
	mIndexAge = findIndexInHeader("microsim.init.age", headerLine);
	mIndexRace = findIndexInHeader("microsim.init.race", headerLine);
	mIndexDrinking = findIndexInHeader("microsim.init.drinkingstatus", headerLine);
	mIndexFrequencyLevel = findIndexInHeader("microsim.init.drink.frequency", headerLine);
	//mIndexMonthlyDrinks = findIndexInHeader("microsim.init.drinks.per.month", headerLine);
	mIndexGpd = findIndexInHeader("microsim.init.gpd", headerLine);
	mIndexMaritalStatus = findIndexInHeader("microsim.roles.marital.status", headerLine);
	mIndexParenthoodStatus = findIndexInHeader("microsim.roles.parenthood.status", headerLine);
	mIndexEmploymentStatus = findIndexInHeader("microsim.roles.employment.status", headerLine);
	mIndexIncome = findIndexInHeader("microsim.init.income", headerLine);

	//read in variables and create agents
	int tempId;
	bool sex;
	int age;
	std::string race;
	int drinking;
	int drinkFrequencyLevel;
	int gpd;
	int monthlyDrinks;
	int maritalStatus;
	int parenthoodStatus;
	int employmentStatus;
	int income;

	//ignore first line, and assume the rest of lines = countOfAgents in model.props
	while (!infoTable.empty() && context.size() < countOfAgents) {
		std::vector<string> info = infoTable.front();
		infoTable.pop();

		//read the variables from a row of infoTable
		tempId = repast::strToInt(info[mIndexId]);
		sex = repast::strToInt(info[mIndexSex]);
		age = repast::strToInt(info[mIndexAge]);
		race = info[mIndexRace];
		drinking = repast::strToInt(info[mIndexDrinking]);
		drinkFrequencyLevel = repast::strToInt(info[mIndexFrequencyLevel]);
		//monthlyDrinks = repast::strToInt(info[mIndexMonthlyDrinks]);
		gpd = repast::strToInt(info[mIndexGpd]);
		monthlyDrinks = (gpd*30)/14;
		maritalStatus = repast::strToInt(info[mIndexMaritalStatus]);
		parenthoodStatus = repast::strToInt(info[mIndexParenthoodStatus]);
		employmentStatus = repast::strToInt(info[mIndexEmploymentStatus]);
		income = repast::strToInt(info[mIndexIncome]);
		int year = simYear + startYear;

		//create an agent using a constructor with variables from file
		int rank = repast::RepastProcess::instance()->rank();
		repast::AgentId id(tempId, rank, 0);
		id.currentRank(rank);
		Agent *agent = new Agent(id, sex, age, race, maritalStatus, parenthoodStatus, employmentStatus, income, drinking,
				drinkFrequencyLevel, monthlyDrinks, monthlyDrinksSDPct, year);
		context.addAgent(agent);

		//init mediators and theory
		initMediatorAndTheoryFromFile(agent, info);
	}

	//Init agents complete. Overwrite infoTable with spawn file data
	getReadyToSpawn();

	//Annual data collection
	initStatisticCollector();

	std::string fileOutputName(ANNUAL_DATA_FILE);
	repast::SVDataSetBuilder builder(fileOutputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());

	StatDataSource<int>* outSumPopulation = mpCollector->getDataSource<int>("Population");
	builder.addDataSource(repast::createSVDataSource("Population", outSumPopulation, std::plus<int>()));

	StatDataSource<int>* outSumMale = mpCollector->getDataSource<int>("Male");
	builder.addDataSource(repast::createSVDataSource("Male", outSumMale, std::plus<int>()));

	StatDataSource<int>* outSumFemale = mpCollector->getDataSource<int>("Female");
	builder.addDataSource(repast::createSVDataSource("Female", outSumFemale, std::plus<int>()));

	StatDataSource<int>* outSumAgeGroup1 = mpCollector->getDataSource<int>("AgeGroup1");
	builder.addDataSource(repast::createSVDataSource("AgeGroup1", outSumAgeGroup1, std::plus<int>()));

	StatDataSource<int>* outSumAgeGroup2 = mpCollector->getDataSource<int>("AgeGroup2");
	builder.addDataSource(repast::createSVDataSource("AgeGroup2", outSumAgeGroup2, std::plus<int>()));

	StatDataSource<int>* outSumAgeGroup3 = mpCollector->getDataSource<int>("AgeGroup3");
	builder.addDataSource(repast::createSVDataSource("AgeGroup3", outSumAgeGroup3, std::plus<int>()));

	StatDataSource<int>* outSumAgeGroup4 = mpCollector->getDataSource<int>("AgeGroup4");
	builder.addDataSource(repast::createSVDataSource("AgeGroup4", outSumAgeGroup4, std::plus<int>()));


	StatDataSource<int>* outSum12MonthDrinkers = mpCollector->getDataSource<int>("12MonthDrinkers");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkers", outSum12MonthDrinkers, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersMale = mpCollector->getDataSource<int>("12MonthDrinkersMale");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersMale", outSum12MonthDrinkersMale, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersFemale = mpCollector->getDataSource<int>("12MonthDrinkersFemale");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersFemale", outSum12MonthDrinkersFemale, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersAgeGroup1 = mpCollector->getDataSource<int>("12MonthDrinkersAgeGroup1");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup1", outSum12MonthDrinkersAgeGroup1, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersAgeGroup2 = mpCollector->getDataSource<int>("12MonthDrinkersAgeGroup2");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup2", outSum12MonthDrinkersAgeGroup2, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersAgeGroup3 = mpCollector->getDataSource<int>("12MonthDrinkersAgeGroup3");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup3", outSum12MonthDrinkersAgeGroup3, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersAgeGroup4 = mpCollector->getDataSource<int>("12MonthDrinkersAgeGroup4");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup4", outSum12MonthDrinkersAgeGroup4, std::plus<int>()));


	StatDataSource<double>* outSumQuantMale = mpCollector->getDataSource<double>("QuantMale");
	builder.addDataSource(repast::createSVDataSource("QuantMale", outSumQuantMale, std::plus<double>()));

	StatDataSource<double>* outSumQuantFemale = mpCollector->getDataSource<double>("QuantFemale");
	builder.addDataSource(repast::createSVDataSource("QuantFemale", outSumQuantFemale, std::plus<double>()));

	StatDataSource<double>* outSumQuantAgeGroup1 = mpCollector->getDataSource<double>("QuantAgeGroup1");
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup1", outSumQuantAgeGroup1, std::plus<double>()));

	StatDataSource<double>* outSumQuantAgeGroup2 = mpCollector->getDataSource<double>("QuantAgeGroup2");
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup2", outSumQuantAgeGroup2, std::plus<double>()));

	StatDataSource<double>* outSumQuantAgeGroup3 = mpCollector->getDataSource<double>("QuantAgeGroup3");
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup3", outSumQuantAgeGroup3, std::plus<double>()));

	StatDataSource<double>* outSumQuantAgeGroup4 = mpCollector->getDataSource<double>("QuantAgeGroup4");
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup4", outSumQuantAgeGroup4, std::plus<double>()));


	StatDataSource<int>* outSumFreqMale = mpCollector->getDataSource<int>("FreqMale");
	builder.addDataSource(repast::createSVDataSource("FreqMale", outSumFreqMale, std::plus<int>()));

	StatDataSource<int>* outSumFreqFemale = mpCollector->getDataSource<int>("FreqFemale");
	builder.addDataSource(repast::createSVDataSource("FreqFemale", outSumFreqFemale, std::plus<int>()));

	StatDataSource<int>* outSumFreqAgeGroup1 = mpCollector->getDataSource<int>("FreqAgeGroup1");
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup1", outSumFreqAgeGroup1, std::plus<int>()));

	StatDataSource<int>* outSumFreqAgeGroup2 = mpCollector->getDataSource<int>("FreqAgeGroup2");
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup2", outSumFreqAgeGroup2, std::plus<int>()));

	StatDataSource<int>* outSumFreqAgeGroup3 = mpCollector->getDataSource<int>("FreqAgeGroup3");
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup3", outSumFreqAgeGroup3, std::plus<int>()));

	StatDataSource<int>* outSumFreqAgeGroup4 = mpCollector->getDataSource<int>("FreqAgeGroup4");
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup4", outSumFreqAgeGroup4, std::plus<int>()));


	StatDataSource<int>* outSumOccasionalHeavyDrinking = mpCollector->getDataSource<int>("SumOccasionalHeavyDrinking");
	builder.addDataSource(repast::createSVDataSource("SumOccasionalHeavyDrinking", outSumOccasionalHeavyDrinking, std::plus<int>()));

	addTheoryAnnualData(builder);

	annualValues = builder.createDataSet();

	initNetwork();
}

void Model::initStatisticCollector() {
	mpCollector = new StatisticsCollector(&context);
}

void Model::getReadyToSpawn() {
	//read in file for spawning info table
	int rank = repast::RepastProcess::instance()->rank();
	std::string rankFileNameProperty = "spawn.file.rank" + std::to_string(rank);
	std::string rankFileName = props->getProperty(rankFileNameProperty);
	std::string spawnFileName = rankFileName;
	readRankFileForTheory(spawnFileName);

	//find index from the header line, ASSUMING the infoTable was already be read
	std::vector<string> headerLine = infoTable.front();
	infoTable.pop();

	mIndexId = findIndexInHeader("microsim.init.id", headerLine);
	mIndexSex = findIndexInHeader("microsim.init.sex", headerLine);
	mIndexAge = findIndexInHeader("microsim.init.age", headerLine);
	mIndexRace = findIndexInHeader("microsim.init.race", headerLine);
	mIndexDrinking = findIndexInHeader("microsim.init.drinkingstatus", headerLine);
	mIndexFrequencyLevel = findIndexInHeader("microsim.init.drink.frequency", headerLine);
	//mIndexMonthlyDrinks = findIndexInHeader("microsim.init.drinks.per.month", headerLine);
	mIndexGpd = findIndexInHeader("microsim.init.gpd", headerLine);
	mIndexMaritalStatus = findIndexInHeader("microsim.roles.marital.status", headerLine);
	mIndexParenthoodStatus = findIndexInHeader("microsim.roles.parenthood.status", headerLine);
	mIndexEmploymentStatus = findIndexInHeader("microsim.roles.employment.status", headerLine);
	mIndexIncome = findIndexInHeader("microsim.init.income", headerLine);
	mIndexSpawnTick = findIndexInHeader("microsim.spawn.tick", headerLine);
}

void Model::doSituationalMechanisms(){
	//Rank 0 checks elapsed duration and throws an error if needed
	if (SINGLE_RUN_LIMIT_MIN > 0) {
		if (repast::RepastProcess::instance()->rank() == 0) {
			auto elapsed = std::chrono::duration_cast<std::chrono::minutes>( std::chrono::steady_clock::now() - startTime );
			if (elapsed.count() > SINGLE_RUN_LIMIT_MIN) {
				std::cerr << "Model. The simulation has run for more than " << SINGLE_RUN_LIMIT_MIN << " minutes." << std::endl;
				throw MPI::Exception(MPI::ERR_QUOTA);
			}
		}
	}

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		//Here is where the agents are looped, agent memories can be revised here
		//Both situational mechanisms and the action mechanism are called
		//Agents update in a random order

		(*iter)->doSituation();
		
		iter++;
	}
	
	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::doActionMechanisms(){
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {

		(*iter)->doAction();

		iter++;
	}
	
	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::doTransformationalMechanisms() {
	std::vector<StructuralEntity*>::iterator it = structuralEntityList.begin();
	while(it != structuralEntityList.end()){

		(*it)->doTransformation();
		
		it++;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}

void Model::ageAgents(){
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		//Age each agent in turn

		(*iter)->ageAgent();
		(*iter)->reset12MonthDrinker();
		(*iter)->resetTotalDrinksPerAnnum();

		iter++;
    }

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::killAgents(){
	// int year = 0;
	// bool agentSex = FEMALE;
	// int agentAge = 0;
	// std::string agentRace = "";

	// std::string hashkey;
	double agentMortality = 0.0;
	bool agentLives = false;
	double migrationOutRate = 0.0;
	bool agentMigration = false;

	std::vector<repast::AgentId> agentKillList;

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();

	while (iter != iterEnd) {
		if ((*iter)->getAge() >= 81) { //kill off agents >= 81 years old
			repast::AgentId id = (*iter)->getId();
			agentKillList.push_back(id);
			countDiePerYear++;
		} else {
			// Get required characteristics
			// year = simYear + startYear;
			// agentSex = (*iter)->getSex();
			// agentAge = (*iter)->getAge();
			// agentRace = (*iter)->getRace();

			// Look up mortality rate
			// hashkey = makeDeathRateHashKey(std::to_string(year), std::to_string(agentSex), std::to_string(agentAge));
			// agentMortality = deathRateTable[hashkey];
			agentMortality = (*iter)->getMortalityRate();
			// Apply risk
			agentLives = (repast::Random::instance()->nextDouble() > agentMortality/365);

			// Flag this agent to die
			if(agentLives == false && countOfAgents > 1){
				repast::AgentId id = (*iter)->getId();
				agentKillList.push_back(id);
				countDiePerYear++;
			} else { //check migration out if this agent is not flagged to kill
				// Look up migration out rate
				// hashkey = makeMigrationOutHashKey(std::to_string(year), std::to_string(agentSex), agentRace, std::to_string(agentAge));
				// migrationOutRate = migrationOutTable[hashkey];
				migrationOutRate = (*iter)->getMigrationOutRate();
				// Apply risk
				agentMigration = (repast::Random::instance()->nextDouble() > migrationOutRate/365);

				// Flag this agent to migrate out
				if(agentMigration == false && countOfAgents > 1){
					repast::AgentId id = (*iter)->getId();
					agentKillList.push_back(id);
					countMigrateOutPerYear++;
				}
			}
		}
		iter++;
	}


	for (repast::AgentId agentId : agentKillList) {
		//remove local copies of this agent in all other agents. TODO: re-check for multi-core removal.
		removeLocalCopies(agentId);

		// Remove agent
		repast::RepastProcess::instance()->agentRemoved(agentId);
		context.removeAgent(agentId);

		// Update counts + records of agents
		countOfAgents--;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::incrementSimYear(){
	simYear++;
	ageAgents();
}

void Model::countSimDay(){
	simDay++; 
	if (simDay==YEARLY_INTERVAL_TICK) simDay = 0;
	//std::cout<<"Day = "<<simDay<<std::endl;
}

void Model::spawnAgents(){
	int tempId;
	bool sex;
	int age;
	std::string race;
	int drinking;
	int drinkFrequencyLevel;
	int gpd;
	int monthlyDrinks;
	double monthlyDrinksSDPct = repast::strToDouble(props->getProperty("microsim.init.monthly.sd.pct"));
	int maritalStatus;
	int parenthoodStatus;
	int employmentStatus;
	int income;
	int year = simYear + startYear;

	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());

	while (!infoTable.empty() && repast::strToInt(infoTable.front()[mIndexSpawnTick]) <= currentTick) {
			std::vector<string> info = infoTable.front();
			infoTable.pop();

			//read the variables from a row of infoTable
			tempId = repast::strToInt(info[mIndexId]);
			sex = repast::strToInt(info[mIndexSex]);
			age = repast::strToInt(info[mIndexAge]);
			race = info[mIndexRace];
			drinking = repast::strToInt(info[mIndexDrinking]);
			drinkFrequencyLevel = repast::strToInt(info[mIndexFrequencyLevel]);
			//monthlyDrinks = repast::strToInt(info[mIndexMonthlyDrinks]);
			gpd = repast::strToInt(info[mIndexGpd]);
			monthlyDrinks = (gpd*30)/14;
			maritalStatus = repast::strToInt(info[mIndexMaritalStatus]);
			parenthoodStatus = repast::strToInt(info[mIndexParenthoodStatus]);
			employmentStatus = repast::strToInt(info[mIndexEmploymentStatus]);
			income = repast::strToInt(info[mIndexIncome]);


			//create an agent using a constructor with variables from file
			int rank = repast::RepastProcess::instance()->rank();
			repast::AgentId id(tempId, rank, 0);
			id.currentRank(rank);
			Agent *agent = new Agent(id, sex, age, race, 
									 maritalStatus, parenthoodStatus, employmentStatus, income,
									 drinking, drinkFrequencyLevel, monthlyDrinks, monthlyDrinksSDPct, year);
			context.addAgent(agent);

			//init mediators and theory
			initMediatorAndTheoryFromFile(agent, info);
			connectSpawnAgentToNetwork(agent);

			//doSituation once when the agent spawn into the simulation
			agent->doSituation();

			// Update counts + records of agents
			countSpawnPerYear++;
			countOfAgents++;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}

// void Model::reset12MonthDrinkers() {
// 	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
// 	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
// 	while (iter != iterEnd) {
// 		//Age each agent in turn

// 		(*iter)->reset12MonthDrinker();

// 		iter++;
// 	}

// 	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
// }

// //added by Hao
// void Model::resetTotalDrinksPerAnnum() {
// 	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
// 	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
// 	while (iter != iterEnd) {
// 		//Age each agent in turn

// 		(*iter)->resetTotalDrinksPerAnnum();

// 		iter++;
// 	}

// 	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
// }

void Model::doRoleTransition() {
	int year;
	int age;
	int sex;
	int currentMarriageStatus;
	int currentParenthoodStatus;
	int currentEmploymentStatus;
	int nextMarriageStatus;
	int nextParenthoodStatus;
	int nextEmploymentStatus;
	int dec_current;
	int dec_next;
	std::vector<double> probabilityRange;//numbers of possible status (hard coded for 3 roles)
	int nextStatus;
	int roleChangeStatus;
	std::string hashkey;
	double tp;
	double sumTP = 0;
	double randomNum;

	//modify transition probability when lead to more roles
	double TPBeta1 = (props->getProperty("role.tp.beta1").empty() ? 0 : repast::strToDouble(props->getProperty("role.tp.beta1")));
	
	//modify transition probability when lead to less roles
	double TPBeta2 = (props->getProperty("role.tp.beta2").empty() ? 0 : repast::strToDouble(props->getProperty("role.tp.beta2")));

	double tempBeta;

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getDayOfRoleTransition()==simDay){
			currentMarriageStatus = (*iter)->getMaritalStatus();
			currentParenthoodStatus = (*iter)->getParenthoodStatus();
			currentEmploymentStatus = (*iter)->getEmploymentStatus();

			//convert binary to decimal
			std::string dec_string = std::to_string(currentMarriageStatus)+ std::to_string(currentParenthoodStatus)+ std::to_string(currentEmploymentStatus);
			dec_current = std::stoi(dec_string,nullptr,2);
			
			//get year, sex, age_group, current status
			year = simYear + startYear;//hard coded for starting from 1980 (put it in model.props?)
			age = (*iter)->getAge();
			sex = (*iter)->getSex();
			probabilityRange.push_back(0);

			//testing roles transition by agent ID
			//repast::AgentId AgentID = (*iter)->getId();
			//if (AgentID.id() == 100)
			//	std::cout<<"Agent "<<"transition day = "<<(*iter)->getDayOfRoleTransition()<<" simDay ="<<simDay<<" M = "<<currentMarriageStatus<<" P = "<<currentParenthoodStatus<<" E = "<<currentEmploymentStatus<<std::endl;

			//create probability ranges for 8 targets
			for (nextMarriageStatus = 0; nextMarriageStatus < 2; nextMarriageStatus++){
				for(nextParenthoodStatus = 0; nextParenthoodStatus < 2; nextParenthoodStatus++){
					for (nextEmploymentStatus = 0; nextEmploymentStatus < 2; nextEmploymentStatus++){
						//making hashkey
						hashkey = makeRoleTPHashKey(std::to_string(year), std::to_string(sex), std::to_string(age), \
												std::to_string(currentMarriageStatus), std::to_string(currentParenthoodStatus), std::to_string(currentEmploymentStatus), \
												std::to_string(nextMarriageStatus), std::to_string(nextParenthoodStatus), std::to_string(nextEmploymentStatus));
					
						tp = roleTransitionProbTable[hashkey];
						//generate decimal for next status
						std::string dec_string = std::to_string(nextMarriageStatus)+ std::to_string(nextParenthoodStatus)+ std::to_string(nextEmploymentStatus);
						dec_next = std::stoi(dec_string,nullptr,2);

						//which beta: more or less
						if (dec_next > dec_current){
							//leading to more roles
							//tp = tp*(1 - TPBeta1*(*iter)->isHaveKDrinksOverNDays(30, 5));
							tempBeta = TPBeta1;
						}
						else if (dec_next < dec_current){
							//leading to less roles
							//tp = tp*(1 + TPBeta2*(*iter)->isHaveKDrinksOverNDays(30, 5));
							tempBeta = TPBeta2;
						}

						//update transition rate
						if ((*iter)->getNumDaysHavingKDrinksOverNDays(30,5) > 0) //heavy drinkers
							tp = tp*(1+tempBeta);
						else
							tp = tp*(1-mAnnualHeavyDrinkingPrev*(1+tempBeta))/(1-mAnnualHeavyDrinkingPrev);

						sumTP = sumTP+tp;
						probabilityRange.push_back(sumTP);
					}
				}
			}
			
			//standardize the probabilityRange vector to [0,1]
			for(int i = 0; i < probabilityRange.size(); i++){
				if (probabilityRange.back() != 0)
					probabilityRange[i] = probabilityRange[i]/probabilityRange.back();
			}

			//generating random value
			randomNum = repast::Random::instance()->nextDouble();

			//getting region a random double value fall in (in [0,1]) and set as next role combi
			for (int index = 1; index < 9; index++){
				if (randomNum > probabilityRange[index-1] && randomNum <= probabilityRange[index]){
					nextStatus = index - 1;

					nextMarriageStatus = nextStatus/4;
					if (nextStatus >= 4) {
						nextParenthoodStatus = (nextStatus - 4) / 2;
						nextEmploymentStatus = (nextStatus - 4) % 2;
					}
					else {
						nextParenthoodStatus = nextStatus / 2;
						nextEmploymentStatus = nextStatus % 2;
					}

					if (dec_current > index - 1)
						roleChangeStatus = -1;
					else if (dec_current < index - 1)
						roleChangeStatus = 1;
					else 
						roleChangeStatus = 0;

					(*iter)->setMaritalStatus(nextMarriageStatus);
					(*iter)->setParenthoodStatus(nextParenthoodStatus);
					(*iter)->setEmploymentStatus(nextEmploymentStatus);
					
					//set up status
					std::string dec_string_next = std::to_string(nextMarriageStatus)+ \
												std::to_string(nextParenthoodStatus)+ \
												std::to_string(nextEmploymentStatus);
					
					dec_next = std::stoi(dec_string_next,nullptr,2);

					if (dec_next == dec_current) (*iter)->setRoleChangeStatus(0);
					else {
						if (dec_next > dec_current) (*iter)->setRoleChangeStatus(1);
						else (*iter)->setRoleChangeStatus(-1);
						(*iter)->setRoleChangedTransient(true);
					}
				
					break;
				}
			}

			probabilityRange.clear();
			sumTP = 0;
		}
		else (*iter)->setRoleChangedTransient(false);

		iter++;
    }
}

void Model::doDailyActions() {
	countSimDay();
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (currentTick==MECHANISM_START_TICK || currentTick % SITUATIONAL_MECHANISM_INTERVAL_TICK == 0) {
		doSituationalMechanisms();
	}
	if (currentTick % ACTION_MECHANISM_INTERVAL_TICK == 0) {
		doActionMechanisms();
	}
	doTransformationalMechanisms();
	if (ROLE_TRANSITION_ON) doRoleTransition();
	if (AGENT_LEVEL_OUTPUT) writeDailyAgentDataToFile();
	killAgents();
	if (SPAWNING_ON) spawnAgents();
}

void Model::doYearlyActions() {
	double dblCurrentTick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
	
	//only collect data on tick = 0
	if (dblCurrentTick == 0) {
		mpCollector->collectAgentStatistics();
		annualValues->record();
		doYearlyTheoryActions();
		if (AGENT_LEVEL_OUTPUT) writeAnnualAgentDataToFile();
	}

	if (dblCurrentTick > 0) {
		mpCollector->collectAgentStatistics();
		annualValues->record();
		if (AGENT_LEVEL_OUTPUT) writeAnnualAgentDataToFile();
		incrementSimYear();
		// reset12MonthDrinkers();
		// resetTotalDrinksPerAnnum();
		doYearlyTheoryActions();
	}

	//reset pop stat count
	countDiePerYear = 0;
	countMigrateOutPerYear = 0;
	countSpawnPerYear = 0;

	//Calculate prevalence of heavy drinking in the population - for role selection
	int countDrinkers = 0;
	int countHeavyDrinkers = 0;
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
			countDrinkers += 1;
			if ((*iter)->getNumDaysHavingKDrinksOverNDays(30,5) > 0) countHeavyDrinkers += 1;
		}
		iter++;
	}
	if (countDrinkers == 0)
		mAnnualHeavyDrinkingPrev = 0;
	else mAnnualHeavyDrinkingPrev = (double)countHeavyDrinkers / (double)countDrinkers;


	if (PRINT_POPULATION_STAT_YEARLY) {
		std::cout << "Year " << simYear << "\t" << countOfAgents
				<< "\tDie " << countDiePerYear
				<< "\tMigrate-Out " << countMigrateOutPerYear
				<< "\tSpawn " << countSpawnPerYear
				<< std::endl;
	}

	//logging memory use
	if (CUSTOM_LOG) {
		int memoryUsed = getUsedMemoryValue();
		if (memoryUsed > maxMemoryUsed) maxMemoryUsed = memoryUsed;
		Logger("Physical memory in use: " + std::to_string(memoryUsed/1024)+"MB");
	}
}

void Model::initSchedule(repast::ScheduleRunner& runner){
	runner.scheduleEvent(MECHANISM_START_TICK, MECHANISM_INTERVAL_TICK, \
			repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doDailyActions)));

	runner.scheduleEvent(YEARLY_START_TICK, YEARLY_INTERVAL_TICK, \
						repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doYearlyActions)));

	runner.scheduleStop(stopAt);

	runner.scheduleEndEvent(repast::Schedule::FunctorPtr(new repast::MethodFunctor<repast::DataSet>(annualValues, &repast::DataSet::write)));
}

//states of a character in CSV file
enum class CSVState {
    UnquotedField,
    QuotedField,
    QuotedQuote
};

//read on row in the CSV file
std::vector<std::string> Model::readCSVRow(const std::string &row) {
    CSVState state = CSVState::UnquotedField;
    std::vector<std::string> fields {""};
    size_t i = 0; // index of the current field
    for (char c : row) {
        switch (state) {
            case CSVState::UnquotedField:
                switch (c) {
                    case ',': // end of field
                              fields.push_back(""); i++;
                              break;
                    case '"': state = CSVState::QuotedField;
                              break;
                    default:  fields[i].push_back(c);
                              break; }
                break;
            case CSVState::QuotedField:
                switch (c) {
                    case '"': state = CSVState::QuotedQuote;
                              break;
                    default:  fields[i].push_back(c);
                              break; }
                break;
            case CSVState::QuotedQuote:
                switch (c) {
                    case ',': // , after closing quote
                              fields.push_back(""); i++;
                              state = CSVState::UnquotedField;
                              break;
                    case '"': // "" -> "
                              fields[i].push_back('"');
                              state = CSVState::QuotedField;
                              break;
                    default:  // end of quote
                              state = CSVState::UnquotedField;
                              break; }
                break;
        }
    }
    return fields;
}

// Read CSV file, Excel dialect. Accept "quoted fields ""with quotes"""
std::queue<std::vector<std::string>> Model::readCSV(std::istream &in) {
    std::queue<std::vector<std::string>> table;
    std::string row;
    while (!in.eof()) {
        std::getline(in, row);
        if (in.bad() || in.fail()) {
            break;
        }
        auto fields = readCSVRow(row);
        table.push(fields);
    }
    return table;
}

void Model::readDeathRateTable(std::string filename) {
	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(filename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << filename << std::endl;
	}

	//find index on header line.
	std::vector<string> headerLine = localTable.front();
	localTable.pop();

	int indexYear = findIndexInHeader("microsim.init.year", headerLine);
	int indexSex = findIndexInHeader("microsim.init.sex", headerLine);
	int indexAge = findIndexInHeader("microsim.init.age", headerLine);
	int indexTP = findIndexInHeader("death.rate", headerLine);

	//read in variables and create map stored in globals.
	std::string localKey;
	double localTP;

	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<string> localMap = localTable.front();
		localTable.pop();

		//read the variables from a row of localTable and make hashkey
		localKey = makeDeathRateHashKey(localMap[indexYear], localMap[indexSex], localMap[indexAge]);
		localTP = repast::strToDouble(localMap[indexTP]);
		//create a key/pair map
		deathRateTable.insert({localKey,localTP});
	}
}

std::string Model::makeDeathRateHashKey(std::string year, std::string sex, std::string age) {
	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = year + sex + age;
	return std::to_string(tp_hash(hash_index));
}

void Model::readMigrationOutTable(std::string filename) {
	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(filename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << filename << std::endl;
	}

	//find index on header line.
	std::vector<string> headerLine = localTable.front();
	localTable.pop();

	int indexYear = findIndexInHeader("microsim.init.year", headerLine);
	int indexSex = findIndexInHeader("microsim.init.sex", headerLine);
	int indexRace = findIndexInHeader("microsim.init.race", headerLine);
	int indexAge = findIndexInHeader("microsim.init.age", headerLine);
	int indexTP = findIndexInHeader("migration.out.rate", headerLine);

	//read in variables and create map stored in globals.
	std::string localKey;
	double localTP;

	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<string> localMap = localTable.front();
		localTable.pop();

		//read the variables from a row of localTable and make hashkey
		localKey = makeMigrationOutHashKey(localMap[indexYear], localMap[indexSex], localMap[indexRace], localMap[indexAge]);
		localTP = repast::strToDouble(localMap[indexTP]);
		//create a key/pair map
		migrationOutTable.insert({localKey,localTP});
	}
}

std::string Model::makeMigrationOutHashKey(std::string year, std::string sex, std::string race, std::string age) {
	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = year + sex + race + age;
	return std::to_string(tp_hash(hash_index));
}
