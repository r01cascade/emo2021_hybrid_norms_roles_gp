#include "Agent.h"
#include "Theory.h"

TheoryMediator::TheoryMediator(std::vector<Theory*> theoryList) {
	mTheoryList = theoryList;
}

TheoryMediator::~TheoryMediator() {
	for(std::vector<Theory*>::iterator iter = mTheoryList.begin(); iter != mTheoryList.end(); iter++) {
		delete (*iter);
	}
}

void TheoryMediator::linkAgent(Agent *agent) {
	//link this mediator to agent
	mpAgent = agent;

	//link each theory to agent
	std::vector<Theory*>::iterator iter;
	for(iter = mTheoryList.begin(); iter != mTheoryList.end(); iter++) {
		(*iter)->setAgent(agent);
	}
}
