/*
 * globals.cpp
 *
 *  Created on: 2017-11-7
 *      Author: alexxparrot
 */

#include "globals.h"
#include <map>
#include <utility>
#include <vector>

bool FAIL_FAST;

const bool MALE = true;
const bool FEMALE = false;
const int NUM_SEX = 2;
const int NUM_AGE_GROUPS = 9;
const int MIN_AGE = 12;
const int MAX_AGE = 100;
const int MAX_DRINKS = 30;
const int AGE_GROUPS[NUM_AGE_GROUPS] =
		{ 14, 24, 34, 44, 54, 64, 74, 84, MAX_AGE };

const int MAX_DRINK_LEVEL = 7;
const int MIN_DRINK_LEVEL = 1;
const int NUM_ROLES_AMMOUNT = 4;

std::map<int, std::pair<double, double> > MEAN_SD_LOOKUP_TABLE;
std::unordered_map<std::string, double> deathRateTable;//storing death rate by characteristics
std::unordered_map<std::string, double> migrationOutTable;//storing migration-out rate by characteristics

double UNIT_PRICE[40] = {
1.75, 1.69, 1.64, 1.64, 1.63, 1.61, 1.64, 1.66,
1.64, 1.63, 1.63, 1.67, 1.72, 1.71, 1.69, 1.65,
1.65, 1.65, 1.66, 1.67, 1.67, 1.65, 1.67, 1.67,
1.67, 1.66, 1.63, 1.63, 1.62, 1.69, 1.67, 1.66,
1.65, 1.64, 1.64, 1.66, 1.66, 1.64, 1.62, 1.63
}; //data from exogenous price table. 1980 through 2019 prices in 2019 dollars.
double TEMP_LEGAL_RISK = 0; //read in from model.props for now.
double DECAY_BASE_RATE = 0; // =0.001 constant from model.props
int DAYS_TO_DEVELOP_WITHDRAWAL = 0; //= 100; constant from model.props
int WITHDRAWAL_WASHOUT_DIVISOR = 0; //=4; constant from model.props
double HOURS_FREE_TIME_MEAN = 0; //4.94 constant from model.props
double HOURS_FREE_TIME_SD = 0; // 3.51 constant from model.props
int TOTAL_YEARS_CONSUMPTION_STOCK = 0; //constant from model.props
int HEAVY_DRINKS_PER_DAY = 0; //constant from model.props




const int THRESHOLD_QUANTITY_HEAVEY_MALE_DRINKER = 30;//gram/day
const int THRESHOLD_QUANTITY_HEAVEY_FEMALE_DRINKER = 20;//gram/day
const int IDFOROUTPUT = 260;
bool ROLES_SOCIALISATION_ON = true;

double WEIGHT_ROLES = 0.5;