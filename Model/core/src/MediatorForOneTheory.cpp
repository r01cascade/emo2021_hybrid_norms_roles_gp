#include "MediatorForOneTheory.h"

#include "TheoryMediator.h"
#include "Theory.h"
#include "globals.h"
#include "Agent.h"

MediatorForOneTheory::MediatorForOneTheory(std::vector<Theory*> theoryList) : TheoryMediator(theoryList) {
	mpTheory = theoryList[0];
}

void MediatorForOneTheory::mediateSituation() {
	mpTheory->doSituation();
}

void MediatorForOneTheory::mediateGatewayDisposition() {
	double mediatedGatewayDouble = mpTheory->doGatewayDisposition();
	mpAgent->setDispositionByIndex(0, mediatedGatewayDouble); // Add first Mediated Disposition to agent 
}

void MediatorForOneTheory::mediateNextDrinksDisposition() {
	std::vector<double> mediatedVector;
	for (int i = 0; i < MAX_DRINKS; ++i){
		mediatedVector.push_back(0);
	}

	std::vector<double> tempVector = mpTheory->doNextDrinksDisposition();		// Calc disposition from theory
	for (int j = 1; j < MAX_DRINKS; ++j) {
		mediatedVector[j] += tempVector[j];		// Assign weighting of disposition and add to vector elements
	}

	for (int i = 1; i < MAX_DRINKS; ++i){
		mpAgent->setDispositionByIndex(i, mediatedVector[i]); // Add Mediated Dispositions to agent dispositions of size MAX_DRINKS
	}
}

void MediatorForOneTheory::mediateNonDrinkingActions(){
	mpTheory->doNonDrinkingActions();
}
