/* Log.h */

#ifndef INCLUDE_LOG_H_
#define INCLUDE_LOG_H_

#include <cstdio>
#include <ctime>
#include <fstream>
#include <cstring>
#include <cstdlib>


inline string getDateAndTime(string s){

    time_t now = time(0);
    struct tm  tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    if(s=="currentDateAndTime")
        strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
    else if(s=="dateOnly")
        strftime(buf, sizeof(buf), "%Y-%m-%d", &tstruct);
    return string(buf);
};

inline void Logger(string logMsg){

    string filePath = "log_"+getDateAndTime("dateOnly")+".txt";
    string now = getDateAndTime("currentDateAndTime");
    ofstream ofs(filePath.c_str(), std::ios_base::out | std::ios_base::app );
    ofs << now << '\t' << logMsg << '\n';
    ofs.close(); 
}

inline int parseLine(char* line){

    int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

//unit in KB
inline int getUsedMemoryValue(){ 

    FILE* file = fopen("/proc/self/status", "r");
    int memoryValue = -1;
    char line[128];
    
    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmRSS:", 6) == 0){
            memoryValue = parseLine(line);
            break;
        }
    }
    fclose(file);
    return memoryValue;
}

#endif