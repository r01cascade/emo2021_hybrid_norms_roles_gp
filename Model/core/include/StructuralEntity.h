#ifndef INCLUDE_STRUCTURALENTITY_H_
#define INCLUDE_STRUCTURALENTITY_H_

#include "Regulator.h"
#include "vector"

class StructuralEntity {

public:
	StructuralEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList, int transformationalInterval) {
		mpRegulatorList = regulatorList;
		mpPowerList = powerList;
		mTransformationalInterval = transformationalInterval;
	};

	virtual ~StructuralEntity() {
		for(std::vector<Regulator*>::iterator iter = mpRegulatorList.begin(); iter != mpRegulatorList.end(); iter++) {delete (*iter);}
	};

	virtual void doTransformation() = 0;

	int mTransformationalTriggerCount = 0; //for count no transformational trigger
	
	//getters
	std::vector<double> getPowerList(){return mpPowerList;}
	int getTransformationalInterval(){return mTransformationalInterval;}

	//setters
	void setPowerList(std::vector<double> vdInput){mpPowerList=vdInput;}
	void setTransformationalInterval(int iInput){mTransformationalInterval=iInput;} 

protected:
	std::vector<Regulator*> mpRegulatorList;
	std::vector<double> mpPowerList;

	int mTransformationalInterval;

};

#endif /* INCLUDE_STRUCTURALENTITY_H_ */
