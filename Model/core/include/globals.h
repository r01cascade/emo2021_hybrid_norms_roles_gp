#ifndef INCLUDE_GLOBALS_H_
#define INCLUDE_GLOBALS_H_
#include <map>
#include <utility>
#include <vector>
#include <unordered_map>

extern bool FAIL_FAST;

extern const bool MALE;
extern const bool FEMALE;
extern const int NUM_SEX;
extern const int NUM_AGE_GROUPS;
extern const int MIN_AGE;
extern const int MAX_AGE;
extern const int MAX_DRINKS;
extern const int AGE_GROUPS[9];
extern const int NUM_ROLES_AMMOUNT;

extern const int MAX_DRINK_LEVEL;
extern const int MIN_DRINK_LEVEL;

extern std::map<int, std::pair<double, double> > MEAN_SD_LOOKUP_TABLE;
extern std::unordered_map<std::string, double> deathRateTable;//storing death rate by characteristics
extern std::unordered_map<std::string, double> migrationOutTable;//storing migration-out rate by characteristics


//TODO: Move theory-specific global variables out of core

/**** RATIONAL CHOICE ****/
extern double UNIT_PRICE[40];
extern double TEMP_LEGAL_RISK;
extern double DECAY_BASE_RATE;
extern int DAYS_TO_DEVELOP_WITHDRAWAL;
extern int WITHDRAWAL_WASHOUT_DIVISOR; 
extern double HOURS_FREE_TIME_MEAN; 
extern double HOURS_FREE_TIME_SD;
extern int TOTAL_YEARS_CONSUMPTION_STOCK; 
extern int HEAVY_DRINKS_PER_DAY;
/**** RATIONAL CHOICE ****/


/**** CONTAGION ****/
/*
extern const int POTENTIAL_BUDDIES_SIZE;

extern  int NUMBER_X_PROCESS;
extern  int NUMBER_Y_PROCESS;
extern  int ORIGIN_X;
extern  int ORIGIN_Y;
extern  int EXTENT_X;
extern  int EXTENT_Y;
extern  double MEAN_CONTINUOUS_REACH;

extern  double INFLUENCE_BETA;
extern  double SELECTION_BETA;
extern  double OUTDEGREE_BETA;
extern  double RECIPROCITY_BETA;
extern  double PREFERENTIAL_ATTACHMENT_BETA;
extern  double TRANSITIVE_TRIPLES_BETA;
extern  double AGE_SIMILARITY_BETA;
extern  double SEX_SIMILARITY_BETA;
extern  double INFLUENCE_LAMBDA;
extern  double SELECTON_LAMBDA;
*/
/**** CONTAGION ****/

/**** ROLES ****/
extern const int THRESHOLD_QUANTITY_HEAVEY_MALE_DRINKER;
extern const int THRESHOLD_QUANTITY_HEAVEY_FEMALE_DRINKER;
extern const int IDFOROUTPUT;
extern bool ROLES_SOCIALISATION_ON;
/**** ROLES ****/

/**** HYBRID ****/
extern double WEIGHT_ROLES;
/**** HYBRID ****/
#endif /* INCLUDE_GLOBALS_H_ */
