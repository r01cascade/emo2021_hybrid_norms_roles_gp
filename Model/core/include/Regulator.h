#ifndef INCLUDE_REGULATOR_H_
#define INCLUDE_REGULATOR_H_

class Regulator {

public:
	virtual ~Regulator() {};

	virtual void updateAdjustmentLevel() = 0; //update the adjustment level variable (private).
	//Modeller should write getter function(s) to access to adjustment level variable, depending on the data structure (it can be a value or array)

};

#endif /* INCLUDE_REGULATOR_H_ */
