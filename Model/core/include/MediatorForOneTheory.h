#ifndef SINGLE_THEORY_MEDIATOR_H_
#define SINGLE_THEORY_MEDIATOR_H_

#include "TheoryMediator.h"
#include "Theory.h"

class MediatorForOneTheory : public TheoryMediator {

private:
	Theory *mpTheory;

public:
	MediatorForOneTheory(std::vector<Theory*> theoryList);

	void mediateSituation() override;
	void mediateGatewayDisposition() override;
	void mediateNextDrinksDisposition() override;
	void mediateNonDrinkingActions() override;
};

#endif /* SINGLE_THEORY_MEDIATOR_H_ */
