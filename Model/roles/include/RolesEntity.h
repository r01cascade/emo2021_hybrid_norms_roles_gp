/* RolesEntity.h */

#ifndef INCLUDE_ROLESENTITY_H_
#define INCLUDE_ROLESENTITY_H_

#include <string>
#include "globals.h"
#include "StructuralEntity.h"
#include "repast_hpc/SharedContext.h"
#include "Agent.h"

class RolesEntity : public StructuralEntity {

private:
	repast::SharedContext<Agent> *mpContext;

	double ***mRoleExpectancyMarriage3DArray;
	double ***mRoleExpectancyParenthood3DArray;
	double ***mRoleExpectancyEmployment3DArray;

	double **mRoleExpectancyMarriage2DArray;
	double **mRoleExpectancyParenthood2DArray;
	double **mRoleExpectancyEmployment2DArray;

	void updateRoleExpectancyArrays();

public:
	//RolesEntity();
    RolesEntity(std::vector<Regulator*> regulatorList, \
				std::vector<double> powerList, \
				int transformationalInterval, \
				//std::vector<std::string> rateofRoleChange,
				std::vector<std::string> roleExpectancyMarriage, \
				std::vector<std::string> roleExpectancyParenthood, \
				std::vector<std::string> roleExpectancyEmployment, \
				repast::SharedContext<Agent> *context
				);
	~RolesEntity();

	int currentDay = 1;
	int currentYear = 0;

	void doTransformation() override;

	double getRoleExpectancyMarriage(int,int);//getting age-sex role expectancy on marriage
	double getRoleExpectancyParenthood(int,int);//getting age-sex role expectancy on marriage
	double getRoleExpectancyEmployment(int,int);//getting age-sex role expectancy on marriage

	int getCurrentYear(){return currentYear;}//provide current year value to theory class

	int*** createInt3DArray(int,int,int,std::vector<std::string>);
	double*** createDouble3DArray(int,int,int,std::vector<std::string>);

	void*** deleteInt3DArray(int,int,int ***Array);
	void*** deleteDouble3DArray(int,int,double ***Array);

	void updateYear();
};

#endif /* INCLUDE_ROLESENTITY_H_ */
