/* RolesTheory.h */

#ifndef ROLES_THEORY_H_
#define ROLES_THEORY_H_

#include "Theory.h"
#include "Agent.h"
#include "repast_hpc/SharedContext.h"
#include "RolesEntity.h"
#include <math.h>

#include <fstream>

class RolesTheory : public Theory {

private:
    //Roles
    //int mEducationStatus;//Todo: design the variable//not considering in Iteration 1
    //double mLevelofInvolvementEducation;//Todo: design the variable//not considering in Iteration 1
    double mLevelofInvolvementMarriage;//value in [0,1] indicating level of involvement
    double mLevelofInvolvementParenthood;//value in [0,1] indicating level of involvement
    double mLevelofInvolvementEmployment;//value in [0,1] indicating level of involvement

    //Roles character
    double mAbilityofCopingRoleStrain;//double number [0,1] to indicate the degree of ability to cope role strain
    //double mRolesDiscontinuity;//Todo: design algorithm to calculate//not considering in Iteration 1
    double mRoleStrain;//double number (0,1) to indicate the degree of ability to cope role strain
    double mRoleSkill;//double number (0,1) read in from model.props indicating the agent’s ability to cope with multiple roles
    double mRoleLoad;//Calculating result of role held and role involvement
    double mRoleIncongruence;//Calculating result of role held and role expected at the age

    //Age(group)-sex role expectancies
    double mRoleExpectancyMarriage;
    double mRoleExpectancyParenthood;
    double mRoleExpectancyEmployment;

    //Alcohol related parameters
    //double mOpportunity;//double number (0,1) indicating the agent’s overall opportunity to engage in drinking
    double mBaselineOpportunity;//double number (0,1)
    double mBalanceOpportunity;//double number (0,1)
    double mOpportunity;//double number (0,1)
    //double mAlphaInOpporutnityCalculation;//double number [0,1] parameter in calculating opportunity to drink
    int mDrinkingLevel;//int number indicating the agent's drinking level (from mIs12MonthDrinker)
    double mProbOppIn;
    double mProbOppOut;

    //role socialisation
    double mBetaSocialisationSpeed;//iteration2 - control speed of role socialisation ranging from 1 to 12
    int mDaysOfSocialisation = 0;//iteration2 - number of days on socialisation
    int mStateOfRoleChange = 0;//iteration2 - recording the state of role changing
    bool mStartSocialising = false;//iteration2 - marking socialisation process starting/stopping
    double mSocialisationSpeed = 0; //iteration2 - socialisation speed - (0,365)

    //Calculation parameters
    //int mExpRoleOverLoad;//exponent of calculating role overload read from model.props file
    const int MAX_INCONGRUENCE = 3;
    double mRoleLoadBeta1;//iteration2
    double mRoleLoadBeta2;//iteration2
    double mRoleLoadBeta3;//iteration2
    double mRoleLoadBeta4;//iteration2
    double mRoleDrinkingOpportunityBeta1;//iteration2
    double mRoleDrinkingOpportunityBeta2;//iteration2
    double mRoleDrinkingOpportunityBeta3;//iteration2
    double mRoleDrinkingOpportunityBeta3b;//iteration2
    double mRoleDrinkingOpportunityBeta4;//iteration2
    double mRoleStrainAffectingGatewayDispositionBeta;//iteration2
    double mRoleStrainAffectingNextDrinkDispositionBeta;//iteration2

    double mTendencyToDrinkInResponseToStrain = 1;//iteration2

    //Disposition parameters
    std::vector<double> mDispositionVector;
    std::vector<double> mDispositionVectorOld;//iteration2 - recording the old disposition
    //double mDispositionBaselineMean;//for calculating baseline disposition
    //double mDispositionBaselineSD;//for calculating baseline disposition
    double mRolesSocialisationBetaMoreRoles;//for revising baseline disposition caused by role socialization
    double mRolesSocialisationBetaLessRoles;//for revising baseline disposition caused by role socialization
    //double mDispositionBetaAffectedByRoleStrainMale;//for revising disposition cause by role strain
    //double mDispositionBetaAffectedByRoleStrainFemale;//for revising disposition cause by role strain
    double mGatewayDispositionInitialization;//for initializing the gateway disposition
    bool mDispositionInitialized = false;

    repast::SharedContext<Agent> *mpPopulation;
    RolesEntity *mpRolesEntity;

    //int compareToMe(Agent* pOtherAgent);

    /* Situational mechanisms */
    void calRoleLoad();
    void calRoleIncongruence();
    void calRoleStrain();
    void calRoleSocialization();
    void calOpportunity();
    void calBaselineDisposition();

    /*Utility functions*/
    int findAgeGroup();

    std::ofstream agentTrackingOutput; //for tracking specific agents

public:
    RolesTheory();
	RolesTheory(repast::SharedContext<Agent> *pPopulation, RolesEntity *pRolesEntity);
	RolesTheory(repast::SharedContext<Agent> *pPopulation, \
                RolesEntity *pRolesEntity, \
                //int educationStatus, //not considering in Iteration 1
                //double levelofInvolvementEducation, //not considering in Iteration 1
                double levelofInvolvementMarriage, \
                double levelofInvolvementParenthood, \
                double levelofInvolvementEmployment, \
                double abilityofCopingRoleStrain, \
                //int expRoleOverload,
                double roleSkill, \
                double gatewayDispositionInitialization, \
                //double rolesDispositionBaselineMean,
                //double rolesDispositionBaselineSD,
                double roleLoadBeta1, \
                double roleLoadBeta2, \
                double roleLoadBeta3, \
                double roleLoadBeta4, \
                double roleDrinkingOpportunityBeta1, \
                double roleDrinkingOpportunityBeta2, \
                double roleDrinkingOpportunityBeta3, \
                double roleDrinkingOpportunityBeta3b, \
                double roleDrinkingOpportunityBeta4, \
                double roleStrainAffectingGatewayDispositionBeta, \
                double roleStrainAffectingNextDrinkDispositionBeta, \
                double rolesSocialisationBetaMoreRoles, \
                double rolesSocialisationBetaLessRoles, \
                //double rolesDispositionBetaAffectedByRoleStrainMale,
                //double rolesDispositionBetaAffectedByRoleStrainFemale,
                double baselineOpportunity, \
                double balanceOpportunity, \
                double mSocialisationSpeed
                //double rolesDiscontinuity, //not considering in Iteration 1
                );
	~RolesTheory();

    void doSituation() override;
    double doGatewayDisposition() override;
    std::vector<double> doNextDrinksDisposition() override;
    void doNonDrinkingActions() override;
};

#endif /* ROLES_THEORY_H_ */
